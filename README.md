Developers can make their messaging and calendar apps more user friendly with NLP using natural language Reminder And Event API. ReminderDrop is a simple natural language processing (NLP) reminder and event detection API. Simple phrases like "how about we get together tomorrow after lunch" get converted to an actionable date and time.
 
See Main.java for usage.