package com.gorny.nlp;

/**
 * Created by maciejg on 10/2/2014.
 */
public interface Rule {
    boolean foundIn(String text, String fullUtterance);
    boolean verifyRole(String text, String context);
    boolean process(Reminder reminder);
    String  expression();
}
