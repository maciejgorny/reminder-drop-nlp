package com.gorny.nlp;

public class nlpProcessor
{
    private NamedEntities.MESSAGE_BODY extractBody  = NamedEntities.MESSAGE_BODY.AFTER_ABOUT_THAT_TO;
    private NamedEntities.TIME_SHIFT timeShift      = NamedEntities.TIME_SHIFT.IN_N;
    private NamedEntities.TIME_SHIFT timeShiftDirect  = NamedEntities.TIME_SHIFT.NEXT_N;
    private NamedEntities.TIME_PLACEMENT tier1      = NamedEntities.TIME_PLACEMENT.DETERMINISTIC_DAY;
    private NamedEntities.TIME_PLACEMENT tier2      = NamedEntities.TIME_PLACEMENT.AMBIGUOUS_TIME;
    private NamedEntities.TIME_PLACEMENT tier3      = NamedEntities.TIME_PLACEMENT.DAY_OF_MONTH;
    private NamedEntities.TIME_PLACEMENT tier3b     = NamedEntities.TIME_PLACEMENT.OF_DAY_OF_MONTH;
    private NamedEntities.TIME_PLACEMENT tier4      = NamedEntities.TIME_PLACEMENT.DAY_OF_WEEK;
    private NamedEntities.TIME_PLACEMENT tier5      = NamedEntities.TIME_PLACEMENT.TIME_OF_DAY;
    private NamedEntities.TIME_OFFSET    tier6      = NamedEntities.TIME_OFFSET.BY_DOW;
    private NamedEntities.TIME_OFFSET    tier7      = NamedEntities.TIME_OFFSET.BY_TIME;
    private NamedEntities.CALL_TO_ACTION action1    = NamedEntities.CALL_TO_ACTION.REMIND_ME_TO;
    private NamedEntities.CALL_TO_ACTION action2    = NamedEntities.CALL_TO_ACTION.TELL_ME_TO;
    private NamedEntities.CALL_TO_ACTION action3    = NamedEntities.CALL_TO_ACTION.DONT_FORGET_TO;
    private NamedEntities.CALL_TO_ACTION action4    = NamedEntities.CALL_TO_ACTION.REMINDER_TO;
    private NamedEntities.CALL_TO_ACTION action5    = NamedEntities.CALL_TO_ACTION.REMEMBER_TO;
    private NamedEntities.RECURRING repeatEvent     = NamedEntities.RECURRING.REPEAT_EVENT;
    private NamedEntities.RECURRING repeatDays      = NamedEntities.RECURRING.DAYS;
    private NamedEntities.TIME_PLACEMENT tier8      = NamedEntities.TIME_PLACEMENT.MONTH_OF_YEAR;

    public Reminder detect(String phrase)
    {
        ReminderRules rules = new ReminderRules(phrase, tz);
        rules
                /**
                    TODO: Case below works for "remind me tonight about that thing tomorrow night" but 
                    effectively also matches "remind me to do something tomorrow at 10:30 pm" thus reverting 
                    body extraction until better rule/role set can be established (do not handle "to" here?)
                 */
                //.apply(extractBody)

                .apply(timeShift)
                .apply(timeShiftDirect)
                .apply(tier1)
                .apply(tier6)
                .apply(tier7)
                .apply(tier2)
                .apply(tier3)
                .apply(tier3b)
                .apply(repeatDays)
                .apply(repeatEvent)
                .apply(tier4)
                .apply(tier5)
                .apply(action1)
                .apply(action2)
                .apply(action3)
                .apply(action4)
                .apply(action5)
                .apply(tier8)
                ;

        return rules.getReminder();
    }

    public nlpProcessor() {
    }
    
    public nlpProcessor(String timeZone) {
        tz = timeZone;
    }
    
    private String tz = java.util.TimeZone.getDefault().getDisplayName();
}
