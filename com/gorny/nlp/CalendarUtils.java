package com.gorny.nlp;

import com.gorny.nlp.parser.AmbiguousTime;

import java.util.Calendar;

public class CalendarUtils
{
    public static enum DAYOFWEEK
    {
        MONDAY      (Calendar.MONDAY),
        TUESDAY     (Calendar.TUESDAY),
        WEDNESDAY   (Calendar.WEDNESDAY),
        THURSDAY    (Calendar.THURSDAY),
        FRIDAY      (Calendar.FRIDAY),
        SATURDAY    (Calendar.SATURDAY),
        SUNDAY      (Calendar.SUNDAY);

        DAYOFWEEK(int day){ this.day = day; }
        private int day = -1;
        public int number(){ return day; }
    }

    public static enum MONTH
    {
        JANUARY     (Calendar.JANUARY),
        FEBRUARY    (Calendar.FEBRUARY),
        MARCH       (Calendar.MARCH),
        APRIL       (Calendar.APRIL),
        MAY         (Calendar.MAY),
        JUNE        (Calendar.JUNE),
        JULY        (Calendar.JULY),
        AUGUST      (Calendar.AUGUST),
        SEPTEMBER   (Calendar.SEPTEMBER),
        OCTOBER     (Calendar.OCTOBER),
        NOVEMBER    (Calendar.NOVEMBER),
        DECEMBER    (Calendar.DECEMBER);

        MONTH(int number) { this.number = number; }
        private int number = -1;
        public int number() { return number; }
    }

    public static enum TIME_UNIT
    {
        SECOND  (Calendar.SECOND),
        MINUTE  (Calendar.MINUTE),
        HOUR    (Calendar.HOUR),
        DAY     (Calendar.DATE),
        WEEK    (Calendar.WEEK_OF_YEAR),
        MONTH   (Calendar.MONTH),
        YEAR    (Calendar.YEAR);

        TIME_UNIT(int unit){ this.unit = unit; }
        private int unit = -1;
        public int unit(){ return unit; }
    }

    public static enum AMBIGUOUS_DAY_TIME
    {
        AFTER_TOMORROW  ("after tomorrow"),
        TOMORROW        ("tomorrow"),
        TODAY           ("today"),
        TONIGHT         ("tonight"),
        MORNING         ("morning"),
        IN_THE_MORNING  ("in the morning"),
        THIS_MORNING    ("this morning"),
        EVENING         ("evening"),
        IN_THE_EVENING  ("in the evening"),
        THIS_EVENING    ("this evening"),
        AT_NIGHT        ("at night"),
        NIGHT           ("night"),
        IN_THE_AFTERNOON("in the afternoon"),
        THIS_AFTERNOON  ("this afternoon"),
        AT_NOON         ("at noon"),
        AFTERNOON       ("afternoon"),
        BEFORE_LUNCH    ("before lunch"),
        AT_LUNCH        ("at lunch"),
        BY_LUNCH        ("by lunch"),
        AT_LUNCH_TIME   ("at lunch time"),
        AFTER_LUNCH     ("after lunch"),
        MIDNIGHT        ("midnight"),
        LATER           ("later")
            ;

        AMBIGUOUS_DAY_TIME(String text){ this.text = text; }
        private String text;
        public String text(){ return text; }
    }

    public static enum NUMBERWORD
    {
        one     (1),
        two     (2),
        three   (3),
        four    (4),
        five    (5),
        six     (6),
        seven   (7),
        eight   (8),
        nine    (9),
        ten     (10),
        eleven  (11),
        twelve  (12);

        private int number;
        NUMBERWORD(int number){ this.number = number; }
        public int number(){ return number; }
    }

    public static String getMonthAsString(int code) {
        String str = null;
        switch (code) {
            case Calendar.JANUARY : str = MONTH.JANUARY.name(); break;
            case Calendar.FEBRUARY : str = MONTH.FEBRUARY.name(); break;
            case Calendar.MARCH : str = MONTH.MARCH.name(); break;
            case Calendar.APRIL : str = MONTH.APRIL.name(); break;
            case Calendar.MAY : str = MONTH.MAY.name(); break;
            case Calendar.JUNE : str = MONTH.JUNE.name(); break;
            case Calendar.JULY : str = MONTH.JULY.name(); break;
            case Calendar.AUGUST : str = MONTH.AUGUST.name(); break;
            case Calendar.SEPTEMBER : str = MONTH.SEPTEMBER.name(); break;
            case Calendar.OCTOBER : str = MONTH.OCTOBER.name(); break;
            case Calendar.NOVEMBER : str = MONTH.NOVEMBER.name(); break;
            case Calendar.DECEMBER : str = MONTH.DECEMBER.name(); break;
        }
        return str;
    }

    public static String getDOWAsString(int code) {
        String str = null;
        switch (code) {
            case Calendar.MONDAY : str = DAYOFWEEK.MONDAY.name(); break;
            case Calendar.TUESDAY : str = DAYOFWEEK.TUESDAY.name(); break;
            case Calendar.WEDNESDAY : str = DAYOFWEEK.WEDNESDAY.name(); break;
            case Calendar.THURSDAY : str = DAYOFWEEK.THURSDAY.name(); break;
            case Calendar.FRIDAY : str = DAYOFWEEK.FRIDAY.name(); break;
            case Calendar.SATURDAY : str = DAYOFWEEK.SATURDAY.name(); break;
            case Calendar.SUNDAY : str = DAYOFWEEK.SUNDAY.name(); break;
        }
        return str;
    }

    /* Function takes a string starting with a number (!) and returns just the numeric part.
     * i.e.: "12th" --> "12"
     */
    public static String NUMBER(String s) {
        return s.split("[a-z]")[0];
    }

    public static void ffwdToNextAmPm(Reminder reminder)
    {
        if (reminder.haveAmPm)
        {
            reminder.date.add(Calendar.DATE, 1);
        }
        else
        {
            Calendar now = Calendar.getInstance();

            int hour = reminder.date.get(Calendar.HOUR_OF_DAY);

            int hourAM = hour < 12 ? hour : hour - 12;
            int hourPM = hour > 12 ? hour : hour + 12;

            int targetMin = reminder.date.get(Calendar.MINUTE);

            boolean isFutureTime = now.before(reminder.date);
            //we want the next valid am/pm time relatve to *now*
            reminder.date.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
            reminder.date.set(Calendar.MINUTE, now.get(Calendar.MINUTE));

            //start with the next hour to avoid premature match if current clock time matches the hour provided
            if (now.after(reminder.date)) //reminder.date.get(Calendar.DATE) == now.get(Calendar.DATE) && !isFutureTime)
                reminder.date.add(Calendar.HOUR_OF_DAY, 1);
            else if (reminder.date.get(Calendar.DATE) > now.get(Calendar.DATE)) {
                reminder.date.set(Calendar.HOUR_OF_DAY, 0);
                reminder.date.set(Calendar.MINUTE, 0);
            }

            System.out.println("ffwdToNextAmPm , hourAM=" + hourAM + ", hourPM=" + hourPM + ", reminder.date.HOUR_OF_DAY=" + reminder.date.get(Calendar.HOUR_OF_DAY));

            while (reminder.date.get(Calendar.HOUR_OF_DAY) != hourPM && reminder.date.get(Calendar.HOUR_OF_DAY) != hourAM
                    /*&& reminder.date.get(Calendar.MINUTE) != targetMin*/) {
                reminder.date.add(Calendar.MINUTE, 1);
                System.out.println("    ffwd time to: " + reminder.date.get(Calendar.HOUR_OF_DAY) + ":" + reminder.date.get(Calendar.MINUTE));
            }

            reminder.date.set(Calendar.MINUTE, targetMin);
        }

    }

    public static void resetTimeIfFutureAndAmbiguous(Reminder reminder) {
        if (isTomorrowOrLater(reminder.date)) {
            reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
            reminder.date.set(Calendar.MINUTE, 0);
            reminder.hint.append(Reminder.Hint.ASK_FOR_TIME);
        }
    }

    /**
     * Advances the status of the recurring reminders from LAPSED to FUTURE status.
     */
    public static void setNextEventForRecurringReminder(Reminder reminder) {
        switch (reminder.repeatTarget) {
            case MINUTES:
                //OP not needed, handled correctly by time shift
                break;
            case HOURS:
                reminder.date.add(Calendar.HOUR_OF_DAY, 1);
                break;
            case DAYS:
                reminder.date.add(Calendar.DATE, reminder.repeatFrequency);
                if (!reminder.haveTime) {
                    reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
                    reminder.date.set(Calendar.MINUTE, AmbiguousTime.NON_ZERO_MINUTES);
                }
                reminder.status = Reminder.ReminderStatus.FUTURE; //move out once all cases are covered
                break;
            case WEEKS:
                reminder.date.add(Calendar.DATE, 7 * reminder.repeatFrequency);
                break;
            case WEEKDAYS:
                break;
            case WEEKEND_DAYS:
                break;
            case MONTHS:
                reminder.date.add(Calendar.MONTH, reminder.repeatFrequency);
                reminder.status = Reminder.ReminderStatus.FUTURE; //move out once all cases are covered
                break;
            case YEARS:
                reminder.date.add(Calendar.YEAR, reminder.repeatFrequency);
                reminder.status = Reminder.ReminderStatus.FUTURE; //move out once all cases are covered
                break;
            case MONDAYS:
                findNextDayOfWeek(reminder, Calendar.MONDAY);
                break;
            case TUESDAYS:
                findNextDayOfWeek(reminder, Calendar.TUESDAY);
                break;
            case WEDNESDAYS:
                findNextDayOfWeek(reminder, Calendar.WEDNESDAY);
                break;
            case THURSDAYS:
                findNextDayOfWeek(reminder, Calendar.THURSDAY);
                break;
            case FRIDAYS:
                findNextDayOfWeek(reminder, Calendar.FRIDAY);
                break;
            case SATURDAYS:
                findNextDayOfWeek(reminder, Calendar.SATURDAY);
                break;
            case SUNDAYS:
                findNextDayOfWeek(reminder, Calendar.SUNDAY);
                break;
        }
        reminder.inflate();
    }

    public static void findNextDayOfWeek(Reminder reminder, int targetDay) {
        reminder.date.add(Calendar.DAY_OF_WEEK, 1);
        while (reminder.date.get(Calendar.DAY_OF_WEEK) != targetDay)
            reminder.date.add(Calendar.DAY_OF_WEEK, 1);
    }

    public static boolean isTomorrowOrLater(Calendar then) {
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DATE, 1);
        boolean isTomorrow =
                        tomorrow.get(Calendar.YEAR) == then.get(Calendar.YEAR) &&
                        tomorrow.get(Calendar.MONTH) == then.get(Calendar.MONTH) &&
                        tomorrow.get(Calendar.DATE) == then.get(Calendar.DATE);
        return isTomorrow || then.after(tomorrow);
    }
}
