package com.gorny.nlp.test;

import com.gorny.nlp.CalendarUtils;
import com.gorny.nlp.Reminder;
import com.gorny.nlp.TimeParser;
import com.gorny.nlp.nlpProcessor;
import com.gorny.nlp.parser.AmbiguousTime;
import com.gorny.nlp.parser.AmbiguousTimeParser;
import com.gorny.nlp.parser.RecurringParser;
import org.testng.Assert;

import java.util.Calendar;
import java.util.Date;

public class allTests
{
    @org.testng.annotations.BeforeMethod
    public void setupTest() {
        Reminder.Hint.NONE.clear();
        Reminder.Hint.ASK_FOR_TIME.clear();
        Reminder.Hint.TIME_UPDATE.clear();
        Reminder.Hint.USE_CALL_TO_ACTION.clear();
        Reminder.Hint.AMBIGUOUS_TIME_OF_DAY.clear();
    }

    //Don't forget to do that thing in 30 minutes

    //remind me every other Thursday to take the garbage out to the curb in the evening

    @org.testng.annotations.Test
    public void testTimeShift5Minutes() throws Exception
    {
        String phrase = "remind me to do something in 5 minutes";
        Calendar expected;
        (expected = Calendar.getInstance()).add(Calendar.MINUTE, 5);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
    }

    @org.testng.annotations.Test
    public void testTimeShiftWithTellMeCallToAction() throws Exception
    {
        String phrase = "tell me to go somewhere in 10 minutes";

        Calendar expected;
        (expected = Calendar.getInstance()).add(Calendar.MINUTE, 10);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "tell me");
        Assert.assertEquals(reminder.text, "go somewhere");
    }

    @org.testng.annotations.Test
    public void testTimeShiftInAMinute() throws Exception
    {
        String phrase = "remind me to do something in a minute";
        Calendar expected;
        (expected = Calendar.getInstance()).add(Calendar.MINUTE, 1);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something"); //TODO: fix trailing punctuation
    }

    @org.testng.annotations.Test
    public void testTimeShif2tHours() throws Exception
    {
        String phrase = "remind me to do something in 2 hours";
        Calendar expected;
        (expected = Calendar.getInstance()).add(Calendar.HOUR, 2);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testTimeShift_InHalfAnHour() throws Exception
    {
        String phrase = "remind me to do something in half an hour";
        Calendar expected;
        (expected = Calendar.getInstance()).add(Calendar.MINUTE, 30);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testTimeShift_InAHalfAnHour() throws Exception
    {
        String phrase = "remind me to do something in a half an hour";
        Calendar expected;
        (expected = Calendar.getInstance()).add(Calendar.MINUTE, 30);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testTimeShift_InHalfHour() throws Exception
    {
        String phrase = "remind me to do something in half hour";
        Calendar expected;
        (expected = Calendar.getInstance()).add(Calendar.MINUTE, 30);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testTimeShift_InAnHour() throws Exception
    {
        String phrase = "remind me to do something in an hour";
        Calendar expected;
        (expected = Calendar.getInstance()).add(Calendar.HOUR, 1);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
    }


    @org.testng.annotations.Test
    public void testTimeShift_HalfHourFromNow() throws Exception
    {
        String phrase = "remind me half hour from now to record the game";
        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, 30);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "record the game");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
    }


    @org.testng.annotations.Test
    public void testNextDayOfWeek_1() throws Exception
    {
        String phrase = "remind me to do something next Friday";
        Calendar expected = Calendar.getInstance();
        //in case today is a Friday, start with the next day
        //and we want NEXT Friday, not THIS Friday
        for(int n=1; n<=2; n++) {
            expected.add(Calendar.DATE, 1);
            while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY)
                expected.add(Calendar.DATE, 1);
        }
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testNextDayOfWeek_2() throws Exception
    {
        String phrase = "remind me next Friday to do something";
        Calendar expected = Calendar.getInstance();
        //in case today is a Friday, start with the next day
        //and we want NEXT Friday, not THIS Friday
        for(int n=1; n<=2; n++) {
            expected.add(Calendar.DATE, 1);
            while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY)
                expected.add(Calendar.DATE, 1);
        }
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
    }

    @org.testng.annotations.Test
    public void testNextDayOfWeekWithTime_1() throws Exception
    {
        String phrase = "remind me to do something next Friday at 9am";
        Calendar expected = Calendar.getInstance();
        //in case today is a Friday, start with the next day
        //and we want NEXT Friday, not THIS Friday
        for(int n=1; n<=2; n++) {
            expected.add(Calendar.DATE, 1);
            while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY)
                expected.add(Calendar.DATE, 1);
        }
        expected.set(Calendar.HOUR, 9);
        expected.set(Calendar.MINUTE, 0);
        expected.set(Calendar.AM_PM, Calendar.AM);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testNextDayOfWeekWithTime_2() throws Exception
    {
        String phrase = "Remind me to do something next Friday at 10 pm";
        Calendar expected = Calendar.getInstance();
        //in case today is a Friday, start with the next day
        //and we want NEXT Friday, not THIS Friday
        for(int n=1; n<=2; n++) {
            expected.add(Calendar.DATE, 1);
            while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY)
                expected.add(Calendar.DATE, 1);
        }
        expected.set(Calendar.HOUR, 10);
        expected.set(Calendar.MINUTE, 0);
        expected.set(Calendar.AM_PM, Calendar.PM);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testNextDayOfWeekWithTime_3() throws Exception
    {
        String phrase = "Remind me to do something next Friday at 8:30 am";
        Calendar expected = Calendar.getInstance();
        //in case today is a Friday, start with the next day
        //and we want NEXT Friday, not THIS Friday
        for(int n=1; n<=2; n++) {
            expected.add(Calendar.DATE, 1);
            while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY)
                expected.add(Calendar.DATE, 1);
        }
        expected.set(Calendar.HOUR, 8);
        expected.set(Calendar.MINUTE, 30);
        expected.set(Calendar.AM_PM, Calendar.AM);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testOnSpecificDayOfTheYear() throws Exception
    {
        String phrase = "remind me to do something on July 15th at 8:30 am";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.MONTH, Calendar.JULY);
        expected.set(Calendar.DAY_OF_MONTH, 15);
        expected.set(Calendar.HOUR, 8);
        expected.set(Calendar.MINUTE, 30);
        expected.set(Calendar.AM_PM, Calendar.AM);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testOnDayOfWeek() throws Exception
    {
        String phrase = "remind me on Monday to do something";
        Calendar expected = Calendar.getInstance();
        //in case today is a Monday, start with the next day
        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.ASK_FOR_TIME);
    }


    @org.testng.annotations.Test
    public void testCallToAction_1() throws Exception
    {
        String phrase = "remind me to do something tomorrow night";
        Calendar expected = Calendar.getInstance();
        //in case today is a Friday, start with the next day
        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
        expected.set(Calendar.MINUTE, 0);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.ASK_FOR_TIME);
    }

    @org.testng.annotations.Test
    public void testCallToAction_2() throws Exception
    {
        String phrase = "remind me tomorrow night to do something";
        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
        expected.set(Calendar.MINUTE, 0);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.hint, Reminder.Hint.ASK_FOR_TIME);
    }

    @org.testng.annotations.Test
    public void testReminderDateTimeNotProvided() throws Exception
    {
        String phrase = "remind me to do something";
        Calendar expected = Calendar.getInstance();
        expected = expectNextTimeOfDay(expected);
        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.ASK_FOR_TIME);
        // status is indeterministic and irrelevant for this test,
        // the value of status depends on how fast Calendar performs time compare and sets it to either LAPSED or INVALID
        Assert.assertNotNull(reminder.status);
    }

    @org.testng.annotations.Test
    public void testReminderNoCallToAction() throws Exception
    {
        String phrase = "do something at 3:30pm";
        Calendar now = Calendar.getInstance();
        Calendar expected = Calendar.getInstance();

        expected.set(Calendar.HOUR, 3);
        expected.set(Calendar.MINUTE, 30);
        expected.set(Calendar.AM_PM, Calendar.PM);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        if (now.after(expected))
            expected.add(Calendar.DATE, 1);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertNull(reminder.callToAction);
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
    }

    @org.testng.annotations.Test
    public void testReminderNotAReminder() throws Exception
    {
        String phrase = "a cow jumped over the moon";
        Calendar expected = Calendar.getInstance();

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertNull(reminder.callToAction);
        Assert.assertEquals(reminder.text, phrase);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
    }


    @org.testng.annotations.Test
    public void testAMPM_1() throws Exception
    {
        String phrase = "remind me to do something tomorrow at 10:30 p.m.";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, 22);
        expected.set(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
    }

    @org.testng.annotations.Test
    public void testAMPM_2() throws Exception
    {
        String phrase = "remind me to do something tomorrow at 9:20 pm.";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, 21);
        expected.set(Calendar.MINUTE, 20);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
    }

    @org.testng.annotations.Test
    public void testAMPM_3() throws Exception
    {
        String phrase = "remind me to do something tomorrow at 7am";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, 7);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
    }

    @org.testng.annotations.Test
    public void testDeterministicDayWithDayOfWeek() throws Exception
    {
        String phrase = "remind me tomorrow night about the meeting on friday";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
        Assert.assertEquals(reminder.text, "the meeting on friday");
    }

    @org.testng.annotations.Test
    public void testAMPM_SpecificTimeAmbiguousAMPM_1() throws Exception
    {
        String phrase = "remind me to do something at 10:30 in the evening tomorrow";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, 22);
        expected.set(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
    }


    @org.testng.annotations.Test
    public void testAMPM_SpecificTimeAmbiguousAMPM_2() throws Exception
    {
        String phrase = "remind me to do something at 5:30 tomorrow morning";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, 5);
        expected.set(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
    }

    @org.testng.annotations.Test
    public void testComplexSentence_AmbiguousDay() throws Exception
    {
        String phrase = "remind me at lunch time that we need to do something tomorrow"; //TODO: <time> before <day>=do not set <day> (?).. relative word position?

        Calendar now = Calendar.getInstance();
        now.setTime(new Date());

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        if (now.get(Calendar.HOUR) < AmbiguousTime.NOON)
        {
            Calendar expected = Calendar.getInstance();
            expected.add(Calendar.DAY_OF_MONTH, 1);
            expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NOON);
            expected.set(Calendar.MINUTE, 0);
            verifyDateTimeResult(reminder.date, expected);

        } else
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.LAPSED);

        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
    }

    @org.testng.annotations.Test
    public void testComplexSentence_AmbiguousDay_BeSpecific() throws Exception
    {
        String phrase = "remind me today at lunch time that we need to do something tomorrow";

        Calendar now = Calendar.getInstance();
        now.setTime(new Date());

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NOON);
        expected.set(Calendar.MINUTE, 0);

        if (now.get(Calendar.HOUR_OF_DAY) == 23) { //ref: DeterministicDayParser#TODAY
            expected.add(Calendar.DAY_OF_MONTH, 1);
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        } else if (now.get(Calendar.HOUR_OF_DAY) > AmbiguousTime.NOON ||
                   (now.get(Calendar.HOUR_OF_DAY) == AmbiguousTime.NOON && now.get(Calendar.MINUTE)>0)) {
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.LAPSED);
        } else {
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        }
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
    }

    @org.testng.annotations.Test
    public void testComplexSentence_AmbiguousTime() throws Exception {
        String phrase = "remind me in the morning to do something tomorrow";

        Calendar now = Calendar.getInstance();
        now.setTime(new Date());

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.ASK_FOR_TIME);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }


    @org.testng.annotations.Test
    public void testTimeAndNonAMPMTimeOfDayDesignation() throws Exception {
        String phrase = "remind me to do something at 10:35 in the evening tomorrow";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, 22);
        expected.set(Calendar.MINUTE, 35);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertNotNull(reminder.callToAction);
        Assert.assertNotNull(reminder.text);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void testStringNormalization_QuarterToN() throws Exception {
        String phrase = "remind me to get going quarter to 10";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        int ampm = TimeParser.determineIntendedTimeOfDay(9,45, reminder); //quarter to 10 == 9:45

        Calendar expected = Calendar.getInstance();
        if (ampm == Calendar.AM)
            expected.set(Calendar.HOUR_OF_DAY, 9);
        else if (ampm == Calendar.PM)
            expected.set(Calendar.HOUR_OF_DAY, 21);

        expected.set(Calendar.MINUTE, 45);

        //adjust for the next day if crossed pm->am boundary,
        //corssing am->pm is fine because we're stayin within the same day
        if (ampm != Calendar.getInstance().get(Calendar.AM_PM)
                && Calendar.getInstance().get(Calendar.AM_PM)==Calendar.PM)
            expected.add(Calendar.DAY_OF_MONTH, 1);

        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testStringNormalization_At_QuarterToN_1() throws Exception {
        String phrase = "remind me to do something at quarter to 10";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        int ampm = TimeParser.determineIntendedTimeOfDay(9,45, reminder); //quarter to 10 == 9:45

        Calendar expected = Calendar.getInstance();
        if (ampm == Calendar.AM)
            expected.set(Calendar.HOUR_OF_DAY, 9);
        else if (ampm == Calendar.PM)
            expected.set(Calendar.HOUR_OF_DAY, 21);
        expected.set(Calendar.MINUTE, 45);

        //adjust for the next day if crossed pm->am boundary,
        //corssing am->pm is fine because we're stayin within the same day
        if (ampm != Calendar.getInstance().get(Calendar.AM_PM)
                && Calendar.getInstance().get(Calendar.AM_PM)==Calendar.PM)
            expected.add(Calendar.DAY_OF_MONTH, 1);

        verifyDateTimeResult(reminder.date, expected);
    }


    @org.testng.annotations.Test
    public void testStringNormalization_At_QuarterToN_2() throws Exception {
        String phrase = "remind me to get going at quarter to 7";

    nlpProcessor nlp = new nlpProcessor();
    Reminder reminder = nlp.detect(phrase);

    int ampm = TimeParser.determineIntendedTimeOfDay(6,45, reminder);//quarter to 7 == 6:45

    Calendar expected = Calendar.getInstance();
    if (ampm == Calendar.AM)
        expected.set(Calendar.HOUR_OF_DAY, 6);
    else if (ampm == Calendar.PM)
        expected.set(Calendar.HOUR_OF_DAY, 18);
    expected.set(Calendar.MINUTE, 45);

    //adjust for the next day if crossed pm->am boundary,
    //corssing am->pm is fine because we're stayin within the same day
    if (ampm != Calendar.getInstance().get(Calendar.AM_PM)
    && Calendar.getInstance().get(Calendar.AM_PM)==Calendar.PM)
        expected.add(Calendar.DAY_OF_MONTH, 1);

    verifyDateTimeResult(reminder.date, expected);
}

    @org.testng.annotations.Test
    public void testStringNormalization_QuarterAfterN() throws Exception {
        String phrase = "remind me to do something quarter after 10"; //quarter after 10 == 10:15

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        int ampm = TimeParser.determineIntendedTimeOfDay(10,15, reminder);

        Calendar expected = Calendar.getInstance();
        if (ampm == Calendar.AM)
            expected.set(Calendar.HOUR_OF_DAY, 10);
        else if (ampm == Calendar.PM)
            expected.set(Calendar.HOUR_OF_DAY, 22);
        expected.set(Calendar.MINUTE, 15);

        //adjust for the next day if crossed pm->am boundary,
        //corssing am->pm is fine because we're stayin within the same day
        if (ampm != Calendar.getInstance().get(Calendar.AM_PM)
                && Calendar.getInstance().get(Calendar.AM_PM)==Calendar.PM)
            expected.add(Calendar.DAY_OF_MONTH, 1);

        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void testStringNormalization_At_QuarterAfterN() throws Exception {
        String phrase = "remind me to do something at quarter after 10"; //quarter after 10 == 10:15

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        int ampm = TimeParser.determineIntendedTimeOfDay(10,15, reminder);

        Calendar expected = Calendar.getInstance();
        if (ampm == Calendar.AM)
            expected.set(Calendar.HOUR_OF_DAY, 10);
        else if (ampm == Calendar.PM)
            expected.set(Calendar.HOUR_OF_DAY, 22);
        expected.set(Calendar.MINUTE, 15);

        //adjust for the next day if crossed pm->am boundary,
        //corssing am->pm is fine because we're stayin within the same day
        if (ampm != Calendar.getInstance().get(Calendar.AM_PM)
                && Calendar.getInstance().get(Calendar.AM_PM)==Calendar.PM)
            expected.add(Calendar.DAY_OF_MONTH, 1);

        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void test_useWordsForNumbers_1() throws Exception {
        String phrase = "remind me to do something at ten to five";  //TODO: test fails at the 5 oclock edge

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        int ampm = TimeParser.determineIntendedTimeOfDay(4,50, reminder); //10 to 5 == 4:50

        Calendar expected = Calendar.getInstance();
        if (ampm == Calendar.AM)
            expected.set(Calendar.HOUR_OF_DAY, 4);
        else if (ampm == Calendar.PM)
            expected.set(Calendar.HOUR_OF_DAY, 16);
        expected.set(Calendar.MINUTE, 50);

        //adjust for the next day if crossed pm->am boundary,
        //corssing am->pm is fine because we're stayin within the same day
        if (ampm != Calendar.getInstance().get(Calendar.AM_PM)
                && Calendar.getInstance().get(Calendar.AM_PM)==Calendar.PM)
            expected.add(Calendar.DAY_OF_MONTH, 1);

        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void test_useWordsForNumbers_2() throws Exception {
        String phrase = "remind me to do something at ten to five in the afternoon";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        //we expect afternoon time 4 P.M. == 16:00
        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.HOUR_OF_DAY, 16);
        expected.set(Calendar.MINUTE, 50);

        //adjust for the next day if crossed pm->am boundary,
        //crossing am->pm is fine because we're stayin within the same day
        Calendar now = Calendar.getInstance();
        if (now.after(expected))
            expected.add(Calendar.DAY_OF_MONTH, 1);

        verifyDateTimeResult(reminder.date, expected);
    }


    @org.testng.annotations.Test
    public void test_ambiguousTimeShift_CoupleOfMinutes() throws Exception {
        String phrase = "remind me to do something in couple of minutes";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, AmbiguousTime.COUPLE_OF);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void test_ambiguousTimeShift_A_CoupleOfMinutes() throws Exception {
        String phrase = "remind me to do something in a couple of minutes";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, AmbiguousTime.COUPLE_OF);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void test_ambiguousTimeShift_A_FewMinutes() throws Exception {
        String phrase = "remind me to do something in a few minutes";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, AmbiguousTime.FEW);
        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void test_ambiguousTimeShift_SeveralHours() throws Exception {
        String phrase = "remind me to do something in several hours";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.HOUR_OF_DAY, AmbiguousTime.SEVERAL);
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_remindAboutTVShowOnSpecificDayAmbiguousTime() throws Exception {
        String phrase = "remind me about Walking Dead on Sunday evening";
        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "walking dead");
        Assert.assertEquals(reminder.hint, Reminder.Hint.ASK_FOR_TIME);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_remindNameToRecordTheGameMondayNight() throws Exception {
        String phrase = "remind Mary to record the game monday night";
        Calendar expected = Calendar.getInstance();

        /*find the next Monday starting with tomorrow*/
        expected.add(Calendar.DAY_OF_MONTH, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind mary");
        Assert.assertEquals(reminder.text, "record the game");
        Assert.assertEquals(reminder.hint, Reminder.Hint.ASK_FOR_TIME);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }


    @org.testng.annotations.Test
    public void test_remindMeLater() throws Exception {
        String phrase = "remind me later to do something";
        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, AmbiguousTime.LATER);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.ASK_FOR_TIME);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }


    @org.testng.annotations.Test
    public void test_dayOfWeek() throws Exception {
        String phrase = "remind me Sunday evening to watch The Walking Dead";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "watch the walking dead");
        Assert.assertEquals(reminder.hint, Reminder.Hint.ASK_FOR_TIME);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }


    @org.testng.annotations.Test
    public void test_timeShift_Ambiguous_FromNow_1() throws Exception {
        String phrase = "remind me to watch Walking Dead half hour from now";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "watch walking dead");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_timeShift_Ambiguous_FromNow_2() throws Exception {
        String phrase = "remind me to watch Walking Dead half an hour from now";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "watch walking dead");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_timeShift_Ambiguous_FromNow_3() throws Exception {
        String phrase = "remind me to watch Walking Dead 30 minutes from now";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "watch walking dead");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_timeShift_Ambiguous_At_Midnight() throws Exception {
        String phrase = "remind me to do something at midnight";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MIDNIGHT);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_timeShift_Ambiguous_Before_Midnight() throws Exception {
        String phrase = "remind me to do something before midnight";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.HOUR_OF_DAY, 23/*AmbiguousTime.BEFORE_MIDNIGHT_HR*/); //"before" shifts the target time
        expected.set(Calendar.MINUTE, 45/*AmbiguousTime.BEFORE_MIDNIGHT_MN*/);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        //TODO: fix the NLP not to mark it as LAPSED if the condition below happens in production
        Calendar now = Calendar.getInstance();
        if (now.after(expected) &&
                !(now.get(Calendar.DATE)==expected.get(Calendar.DATE) && now.get(Calendar.HOUR_OF_DAY)==23 && now.get(Calendar.MINUTE)<=59)) {
            expected.add(Calendar.DATE, 1);
        }

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
    }

    @org.testng.annotations.Test
    public void test_timeShift_Ambiguous_After_Midnight() throws Exception {

        //
        //FIXME: test fais if run before noon, targets the next 12am/pm which before noon is 12pm, which is wrong, we want MIDNIGHT!
        //

        String phrase = "remind me to do something after midnight";
        Calendar now = Calendar.getInstance();

        Calendar expected = Calendar.getInstance();
        if (now.get(Calendar.HOUR_OF_DAY) > 0 ||
                (now.get(Calendar.HOUR_OF_DAY) == 0 && now.get(Calendar.MINUTE) > AmbiguousTime.AFTER_MIDNIGHT_MN))
            expected.add(Calendar.DATE, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTER_MIDNIGHT_HR);
        expected.set(Calendar.MINUTE, AmbiguousTime.AFTER_MIDNIGHT_MN);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_TimeUpdateHint_N() throws Exception {
        String phrase = "make it 2 p.m.";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.HOUR_OF_DAY, 14);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar now = Calendar.getInstance();
        if (now.after(expected))
            expected.add(Calendar.DATE, 1);

        Assert.assertNull(reminder.callToAction);
        Assert.assertEquals(reminder.text, "make it");
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
    }

    @org.testng.annotations.Test
    public void test_TimeUpdateHint_At_N() throws Exception {
        String phrase = "make it at 2 p.m.";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.HOUR_OF_DAY, 14);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar now = Calendar.getInstance();
        if (now.after(expected))
            expected.add(Calendar.DATE, 1);

        Assert.assertNull(reminder.callToAction);
        Assert.assertEquals(reminder.text, "make it");
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
    }

    @org.testng.annotations.Test
    public void test_timeBrokenUpBySentenceForm() throws Exception {
        String phrase = "Meet me there tomorrow. Let's make it at 2 p.m.";
        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_MONTH, 1);
        expected.set(Calendar.HOUR_OF_DAY, 14);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertNull(reminder.callToAction);
        Assert.assertEquals(reminder.text, "meet me there let's make it");
        Assert.assertEquals(reminder.hint, Reminder.Hint.USE_CALL_TO_ACTION);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.EVENT_DETECTED);
    }

    @org.testng.annotations.Test
    public void test_TimeUpdateHint_NoTimeOfDay() throws Exception {
        String phrase = "make it at 2";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        int ampm = TimeParser.determineIntendedTimeOfDay(2,0, reminder);

        Calendar expected = Calendar.getInstance();
        if (ampm == Calendar.AM)
            expected.set(Calendar.HOUR_OF_DAY, 2);
        else if (ampm == Calendar.PM)
            expected.set(Calendar.HOUR_OF_DAY, 14);
        expected.set(Calendar.MINUTE, 0);

        Assert.assertNull(reminder.callToAction);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.TIME_UPDATE.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
    }

    @org.testng.annotations.Test
    public void test_TimeUpdateHint_AmbiguousTime() throws Exception {
        String phrase = "two";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Assert.assertNull(reminder.callToAction);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
    }

    @org.testng.annotations.Test
    public void test_TimeUpdateHint_NoTimeOfDay_NoText() throws Exception {
        String phrase = "at 2"; //TODO: Fix the test, the test fails between 2:00-3:00

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        int ampm = TimeParser.determineIntendedTimeOfDay(2,0, reminder);

        Calendar expected = Calendar.getInstance();
        if (ampm == Calendar.AM)
            expected.set(Calendar.HOUR_OF_DAY, 2);
        else if (ampm == Calendar.PM)
            expected.set(Calendar.HOUR_OF_DAY, 14);
        expected.set(Calendar.MINUTE, 0);

        Assert.assertNull(reminder.callToAction);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.TIME_UPDATE.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
    }


    @org.testng.annotations.Test
    public void test_TimeUpdateHint_At_QuarterToMidnight() throws Exception {
        String phrase = "remind me to do something at quarter to midnight";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        int ampm = TimeParser.determineIntendedTimeOfDay(23, 45, reminder);

        Calendar expected = Calendar.getInstance();
        if (ampm == Calendar.AM)
            expected.set(Calendar.HOUR_OF_DAY, 11);
        else if (ampm == Calendar.PM)
            expected.set(Calendar.HOUR_OF_DAY, 23);
        expected.set(Calendar.MINUTE, 45);

        Assert.assertNotNull(reminder.callToAction);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertTrue(reminder.hint.isEmpty());
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }


    @org.testng.annotations.Test
    public void test_TimeUpdateHint_At_A_QuarterToMidnight() throws Exception {
        String phrase = "remind me to do something at a quarter to midnight";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        int ampm = TimeParser.determineIntendedTimeOfDay(23,45, reminder);

        Calendar expected = Calendar.getInstance();
        if (ampm == Calendar.AM)
            expected.set(Calendar.HOUR_OF_DAY, 11);
        else if (ampm == Calendar.PM)
            expected.set(Calendar.HOUR_OF_DAY, 23);
        expected.set(Calendar.MINUTE, 45);

        Assert.assertNotNull(reminder.callToAction);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertTrue(reminder.hint.isEmpty());
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_voiceInput_spelledOutNumbers_1() throws Exception {
        String phrase = "remind me to do something tomorrow at five thirty six pm";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, 17);
        expected.set(Calendar.MINUTE, 36);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

    }

    @org.testng.annotations.Test
    public void test_voiceInput_spelledOutNumbers_2() throws Exception {
        String phrase = "remind me to do something tomorrow at five eleven am";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, 5);
        expected.set(Calendar.MINUTE, 11);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_fullDateGivenInText() throws Exception {
        String phrase = "this will take place on November 12th 2018 at 3:30pm";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.YEAR, 2018);
        expected.set(Calendar.MONTH, Calendar.NOVEMBER);
        expected.set(Calendar.DATE, 12);
        expected.set(Calendar.HOUR_OF_DAY, 15);
        expected.set(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void test_shortDayOfWeek() throws Exception {
        String phrase = "Call Jim Mon. at 7:45 pm";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, 19);
        expected.set(Calendar.MINUTE, 45);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
    }

    @org.testng.annotations.Test
    public void test_timeRolesTest() {
        String phrase = "remind me to print out 12 copies for tomorrow's meeting tomorrow at 9:20 am";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, 9);
        expected.set(Calendar.MINUTE, 20);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "print out 12 copies for tomorrow's meeting");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }


    @org.testng.annotations.Test
    public void test_timeRoles_afternoon_as_not_time() {
        String phrase = "remind me tomorrow at 9:20 to print out 12 copies for the afternoon meeting";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, 9);
        expected.set(Calendar.MINUTE, 20);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "print out 12 copies for the afternoon meeting");
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.AMBIGUOUS_TIME_OF_DAY.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_timeRoles_afternoon_as_time() {
        String phrase = "remember to do something nice on Friday afternoon";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTERNOON);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remember to");
        Assert.assertEquals(reminder.text, "do something nice");
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.ASK_FOR_TIME.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_timeRoles_afternoon_as_time_2() {
        String phrase = "remember to do something nice on Friday in the afternoon";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTERNOON);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remember to");
        Assert.assertEquals(reminder.text, "do something nice");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_fullDateGiven() {
        String phrase = "Remember that the event takes place on Nov. 27th 2019 at the Center Arena";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.YEAR, 2019);
        expected.set(Calendar.MONTH, Calendar.NOVEMBER);
        expected.set(Calendar.DATE, 27);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.ASK_FOR_TIME.name()));
        Assert.assertEquals(reminder.callToAction, "remember that");
        Assert.assertEquals(reminder.text, "the event takes place at the center arena");
    }

    @org.testng.annotations.Test
    public void test_fullDateGiven_2() {
        String phrase = "Upcoming some event - On Sun Dec 20th";

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.MONTH, Calendar.DECEMBER);
        expected.set(Calendar.DATE, 20);

        if (reminder.status != Reminder.ReminderStatus.LAPSED) {
            if (CalendarUtils.isTomorrowOrLater(reminder.date)) {
                CalendarUtils.resetTimeIfFutureAndAmbiguous(reminder);
                expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
                expected.set(Calendar.MINUTE, 0);
            }
            else
                expected = expectNextTimeOfDay(expected);
        }
        verifyDateTimeResult(reminder.date, expected);

        /*
            NO_REMINDER_DETECTED if date is in the future, but fails with
            status LAPSED on or after that date. Both outcomes are valid.
         */
        Assert.assertTrue(reminder.status == Reminder.ReminderStatus.NO_REMINDER_DETECTED
                || reminder.status == Reminder.ReminderStatus.LAPSED);

        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.ASK_FOR_TIME.name()));
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "upcoming some event -");
    }

    @org.testng.annotations.Test
    public void test_justBefore_ThisHour() {   //FIXME: test fails after midnight, but the result is correct, the test is wrong
        Calendar now = Calendar.getInstance();

        int N = now.get(Calendar.HOUR);
        String phrase = "This needs to go out just before " + N;

        Calendar expected = Calendar.getInstance();

        if (now.get(Calendar.AM_PM) == Calendar.AM) {
            expected.set(Calendar.HOUR_OF_DAY, N);
            expected.set(Calendar.MINUTE, 0);
            expected.add(Calendar.MINUTE, AmbiguousTime.SHIFT_JUST_BEFORE);
            if (now.after(expected))
                expected.add(Calendar.HOUR, 12);
        } else {
            //in PM, the next available time for the hour just passed MUST be in AM the next day
            expected.add(Calendar.DATE, 1);
            expected.set(Calendar.HOUR_OF_DAY, N);
            expected.set(Calendar.MINUTE, 0);
            expected.add(Calendar.MINUTE, AmbiguousTime.SHIFT_JUST_BEFORE);
        }

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "this needs to go out");
    }

    @org.testng.annotations.Test
    public void test_rightAfter_ThisHour() {      //FIXME: test fails after midnight, but the result is correct, the test is wrong
        Calendar now = Calendar.getInstance();

        /** TODO: WARNING! this test may fail at different times of day. Fix this test case for all times of day.
         * The test fails during time window between a full hour and "right after" the full hour,
         * i.e. "right after N" --> N:03, and the test fails between N:00 - N:03. (fix me pls!), but the NLP is actually correct
         * Tested: at 10 pm --> 10 am next day
         *
         */

        int N = now.get(Calendar.HOUR);
        String phrase = "This needs to go out right after " + N;

        Calendar expected = Calendar.getInstance();

        if (now.get(Calendar.AM_PM) == Calendar.AM) {
            expected.set(Calendar.HOUR_OF_DAY, N + 12); //"just before 9" is "8:56:
            expected.set(Calendar.MINUTE, 0);
            expected.add(Calendar.MINUTE, AmbiguousTime.SHIFT_JUST_AFTER);
        } else {
            //in PM, the next available time for the hour just passed MUST be in AM the next day
            expected.add(Calendar.DATE, 1);
            expected.set(Calendar.HOUR_OF_DAY, N);
            expected.set(Calendar.MINUTE, 0);
            expected.add(Calendar.MINUTE, AmbiguousTime.SHIFT_JUST_AFTER);
        }

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.AMBIGUOUS_TIME_OF_DAY.name()));
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "this needs to go out");
    }

    //FIXME: potentially innacurate unit test - actual outcome is correct
    @org.testng.annotations.Test
    public void test_lapsedTimeRolloverTest() {
        String phrase = "remind me about the shopping trip at 5";

        Calendar now = Calendar.getInstance();
        Calendar expected = Calendar.getInstance();

        if (now.get(Calendar.HOUR_OF_DAY) >= 17) {
            expected.add(Calendar.DATE, 1);
            expected.set(Calendar.HOUR_OF_DAY, 5);
        } else if (now.get(Calendar.HOUR_OF_DAY) < 5) {
            expected.set(Calendar.HOUR_OF_DAY, 5);
        } else {
            expected.set(Calendar.HOUR_OF_DAY, 17);
        }
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.AMBIGUOUS_TIME_OF_DAY.name()));
        Assert.assertEquals(reminder.text, "the shopping trip");
    }

    @org.testng.annotations.Test
    public void test_adjustingTimeOffset_TheNightBefore() {
        String phrase = "remind me the night before that Olivia has her class trip on Tuesday";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.TUESDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.add(Calendar.DAY_OF_WEEK, -1); // "the night before"
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
        expected.set(Calendar.MINUTE, AmbiguousTime.NON_ZERO_MINUTES);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "olivia has her class trip");
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.ASK_FOR_TIME.name()));

        Calendar today = Calendar.getInstance();
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DATE, 1);
        if (tomorrow.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY && today.get(Calendar.HOUR_OF_DAY)>=AmbiguousTime.NIGHT)
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.LAPSED);
        else
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_adjustingTimeOffset_InTheMorning() {
        String phrase = "Remind me in the morning about Olivia's class trip on Tuesday";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.TUESDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "olivia's class trip");
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.ASK_FOR_TIME.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_advanceNotice_30MinutesEarly() {
        String phrase = "Wake me up 30 minutes early before the shopping trip tonight at seven forty five";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.HOUR_OF_DAY, 19);
        expected.set(Calendar.MINUTE, 15);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "wake me up before the shopping trip");

        Calendar now = Calendar.getInstance();
        if (now.before(expected))
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.EVENT_DETECTED);
        else
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.LAPSED);
    }

    @org.testng.annotations.Test
    public void test_usingAtSign_1() {
        String phrase = "call Jim @10am";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.HOUR_OF_DAY, 10);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.HOUR_OF_DAY) >= 10)
            expected.add(Calendar.DATE, 1);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "call jim");
    }

    @org.testng.annotations.Test
    public void test_usingAtSign_2() {
        String phrase = "call Jim @ 9";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        if (now.get(Calendar.HOUR_OF_DAY) < 9) {
            expected.set(Calendar.HOUR_OF_DAY, 9);
            expected.set(Calendar.MINUTE, 0);
        }
        else
        if (now.get(Calendar.HOUR_OF_DAY) >= 9 && now.get(Calendar.HOUR_OF_DAY) < 21) {
            expected.set(Calendar.HOUR_OF_DAY, 21);
            expected.set(Calendar.MINUTE, 0);
        }
        else
        if (now.get(Calendar.HOUR_OF_DAY) >= 21) {
            expected.add(Calendar.DATE, 1);
            expected.set(Calendar.HOUR_OF_DAY, 9);
            expected.set(Calendar.MINUTE, 0);
        }

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "call jim");
    }

    /*
     * End Of Day tests resolve to tomorrow if after END_OF_DAY_HRS, but "end of day today" should resolve to LAPSED if after the target time
     */
    @org.testng.annotations.Test
    public void test_byEndOfDay() {
        String phrase = "remind me to get this done by the end of the day";
        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.END_OF_DAY_HOUR);
        expected.set(Calendar.MINUTE, AmbiguousTime.END_OF_DAY_MINS);

        if (now.get(Calendar.HOUR_OF_DAY) > AmbiguousTime.END_OF_DAY_HOUR ||
                (now.get(Calendar.HOUR_OF_DAY) == AmbiguousTime.END_OF_DAY_HOUR && now.get(Calendar.MINUTE) > AmbiguousTime.END_OF_DAY_MINS)) {
            expected.add(Calendar.DATE, 1);
        }

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "get this done");
    }

    @org.testng.annotations.Test
    public void test_byEndOfDay_Today() {
        /**
         * This test normalizes to "remind me to do this at 16:45 pm today"
         * This test fails if ran between 23:00 and midnight due to us adding an hour to ambiguous "today", which tips the scale over to the next day (we're crossing midnight mark).
         * THEN (!) time parser applies time entity 16:45 pm to an already pre-set day
         */
        String phrase = "remind me to do this towards the end of the day today";//resolved to tomorrow if after END_OF_DAY_HRS, but "end of day today" should resolve to LAPSED
        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.END_OF_DAY_HOUR);
        expected.set(Calendar.MINUTE, AmbiguousTime.END_OF_DAY_MINS);

        verifyDateTimeResult(reminder.date, expected);

        if (now.get(Calendar.HOUR_OF_DAY) > AmbiguousTime.END_OF_DAY_HOUR ||
                (now.get(Calendar.HOUR_OF_DAY) == AmbiguousTime.END_OF_DAY_HOUR && now.get(Calendar.MINUTE)>AmbiguousTime.END_OF_DAY_MINS))
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.LAPSED);
        else
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do this");
    }

    @org.testng.annotations.Test
    public void test_recurring_EveryDayAtN() {
        String phrase = "remind me to do this and that every day at 9 in the evening";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.HOUR_OF_DAY)>=21)
            expected.add(Calendar.DATE, 1);

        expected.set(Calendar.HOUR_OF_DAY, 21);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do this and that");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.DAYS);
    }

    @org.testng.annotations.Test
    public void test_recurring_OnNofEveryMonth() {
        String phrase = "remind me to do this and that on the 20th of every month";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.DATE)>=20)
            expected.add(Calendar.MONTH, 1);
        expected.set(Calendar.DATE, 20);
        expected.set(Calendar.HOUR_OF_DAY, 6);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do this and that");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.MONTHS);
    }


    @org.testng.annotations.Test
    public void test_recurring_every_N_minutes() {
        String phrase = "remind me to stretch my bones every 20 minutes";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, 20);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "stretch my bones");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.MINUTES);
    }

    //the 15th of
    @org.testng.annotations.Test
    public void test_recurring_everyYear_DayOfMonth() {
        String phrase = "remember about Mary's b-day every year on August 15th";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.MONTH, Calendar.AUGUST); //before Aug. 15 of *this* year
        expected.set(Calendar.DATE, 15);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        Calendar now = Calendar.getInstance();
        if (now.after(expected))
            expected.add(Calendar.YEAR, 1); //if after Aug. 15 of *this* year, set reminder to next year Aug. 15

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remember about");
        Assert.assertEquals(reminder.text, "mary's b-day");

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.YEARS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }


    @org.testng.annotations.Test
    public void test_recurring_every_N_months_OnDayOfMonth() {
        String phrase = "remember about Mary's b-day every 2 months on the 1st of the month";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MONTH, 2);
        expected.set(Calendar.DATE, 1);

        expected.set(Calendar.HOUR_OF_DAY, 6);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remember about");
        Assert.assertEquals(reminder.text, "mary's b-day");

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.MONTHS);
        Assert.assertEquals(reminder.repeatFrequency, 2);
    }

    @org.testng.annotations.Test
    public void test_recurring_everyYear_OnDayOfMonth_weekInAdvance() {
        String phrase = "remind me a week in advance about our anniversary every year on Oct 11";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.MONTH, Calendar.OCTOBER); //Oct. 11th - 7 days
        expected.set(Calendar.DATE, 4);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE,  AmbiguousTime.NON_ZERO_MINUTES);

        Calendar now = Calendar.getInstance();
        if (now.after(expected))
            expected.add(Calendar.YEAR, 1); //if after Aug. 15 of *this* year, set reminder to next year Aug. 15

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "our anniversary");

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.YEARS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_recurring_everyYear_OnDayOfMonth_NdaysBefore() {
        String phrase = "remind me two days before the event every year on Oct 11";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.MONTH, Calendar.OCTOBER); //Oct. 11th - 7 days
        expected.set(Calendar.DATE, 9);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE,  AmbiguousTime.NON_ZERO_MINUTES);

        Calendar now = Calendar.getInstance();
        if (now.after(expected))
            expected.add(Calendar.YEAR, 1); //if after Aug. 15 of *this* year, set reminder to next year Aug. 15

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "the event");

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.YEARS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_recurring_everyYear_OnDayOfMonth_NdaysAfter() {
        String phrase = "remind me every year to do this and that three days after the event on Oct 11";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.MONTH, Calendar.OCTOBER); //Oct. 11th + 2 days
        expected.set(Calendar.DATE, 14);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, AmbiguousTime.NON_ZERO_MINUTES);

        Calendar now = Calendar.getInstance();
        if (now.after(expected))
            expected.add(Calendar.YEAR, 1); //if after Aug. 15 of *this* year, set reminder to next year Aug. 15

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do this and that the event");

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.YEARS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_recurring_everyDay_NoTimeGiven() {
        String phrase = "remind me each day that every day is a special day";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.HOUR_OF_DAY) > AmbiguousTime.MORNING ||
                (now.get(Calendar.HOUR_OF_DAY)==AmbiguousTime.MORNING && now.get(Calendar.MINUTE) > AmbiguousTime.NON_ZERO_MINUTES))
            expected.add(Calendar.DATE, 1);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "every day is a special day");

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.DAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_recurring_everyDay_SpecificTimeGiven() {
        String phrase = "remind me each day at 10:15 am that every day is a special day";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.HOUR_OF_DAY)>10 || (now.get(Calendar.HOUR_OF_DAY)==10 && now.get(Calendar.MINUTE) > 15))
            expected.add(Calendar.DATE, 1);

        expected.set(Calendar.HOUR_OF_DAY, 10);
        expected.set(Calendar.MINUTE, 15);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "every day is a special day");

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.DAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_recurring_everyOtherYear_onSpecificDay() {
        String phrase = "remind me six days early about Mary's b-day every other year on August 15th";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        expected.set(Calendar.MONTH, Calendar.AUGUST);
        expected.set(Calendar.DATE, 9); //Aug. 15 - 6 days
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        if (now.get(Calendar.MONTH)>Calendar.AUGUST ||
                (now.get(Calendar.MONTH)==Calendar.AUGUST && now.get(Calendar.DATE)>15))
        expected.add(Calendar.YEAR, 2);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "mary's b-day");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.YEARS);
        Assert.assertEquals(reminder.repeatFrequency, 2);
    }

    @org.testng.annotations.Test
    public void test_recurring_everyOtherDay() {
        String phrase = "tell me to go to the gym every other day at 4pm";

        Calendar now = Calendar.getInstance();
        Calendar expected = Calendar.getInstance();

        if (now.get(Calendar.HOUR_OF_DAY)>=16)
            expected.add(Calendar.DATE, 1);

        expected.set(Calendar.HOUR_OF_DAY, 16);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "tell me");
        Assert.assertEquals(reminder.text, "go to the gym");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.DAYS);
        Assert.assertEquals(reminder.repeatFrequency, 2);
    }

    @org.testng.annotations.Test
    public void test_recurring_DayOfWeek() {
        String phrase = "Watch The Walking Dead Sundays at 9pm";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);
        expected.set(Calendar.HOUR_OF_DAY, 21);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, null);             //TODO: should 'Watch' be a call to action?
        Assert.assertEquals(reminder.text, "watch the walking dead");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED); //no call to action ==> no FUTURE status

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.SUNDAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_timeShift_Ambiguous_AfterLunch() throws Exception {
        String phrase = "tell Dave when he comes in that I need this after lunch";
        Calendar now = Calendar.getInstance();

        Calendar expected = Calendar.getInstance();
        if (now.get(Calendar.HOUR_OF_DAY) > AmbiguousTime.AFTER_LUNCH ||
                (now.get(Calendar.HOUR_OF_DAY) == AmbiguousTime.AFTER_LUNCH && now.get(Calendar.MINUTE) > 0))
            expected.add(Calendar.DATE, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTER_LUNCH);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "tell dave");
        Assert.assertEquals(reminder.text, "when he comes in that i need this");
        Assert.assertEquals(reminder.hint, Reminder.Hint.NONE);
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
    }

    @org.testng.annotations.Test
    public void test_recurring_in_N_Min() {
        String phrase = "we will be done in eight min";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, 8);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.recurring, false);
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "we will be done");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED); //no call to action ==> no FUTURE status
    }


    @org.testng.annotations.Test
    public void test_rollback_time_to_LAPSED() {
        String phrase = "remind me ten minutes early to do something in five minutes";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MINUTE, -5); //in 5 min. - 10 min.

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.recurring, false);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.LAPSED);
    }

    @org.testng.annotations.Test
    public void test_nextWeek() {
        String phrase = "let me know next week how to handle this";

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "let me know how to handle this");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED); //no call to action ==> no FUTURE status

        Assert.assertEquals(reminder.recurring, false);
    }

    @org.testng.annotations.Test
    public void test_recurring_every_TimeOfDay_1() {
        String phrase = "Watch The Walking Dead every evening at 9";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.HOUR_OF_DAY)>=21)
            expected.add(Calendar.DATE, 1);

        expected.set(Calendar.HOUR_OF_DAY, 21);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "watch the walking dead");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED); //no call to action ==> no FUTURE status

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.DAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_recurring_every_TimeOfDay_2() {
        String phrase = "Watch The Walking Dead evenings at 9";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.HOUR_OF_DAY)>=21)
            expected.add(Calendar.DATE, 1);

        expected.set(Calendar.HOUR_OF_DAY, 21);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "watch the walking dead");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED); //no call to action ==> no FUTURE status

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.DAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_recurring_every_TimeOfDay_3() {
        String phrase = "remind me to do this evenings at 9 pm";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.HOUR_OF_DAY)>=21)
            expected.add(Calendar.DATE, 1);

        expected.set(Calendar.HOUR_OF_DAY, 21);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do this");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.DAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_recurring_every_TimeOfDay_4() {
        String phrase = "remind me to do this every other evening at 9 pm";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.HOUR_OF_DAY)>=21)
            expected.add(Calendar.DATE, 1);

        expected.set(Calendar.HOUR_OF_DAY, 21);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do this");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.DAYS);
        Assert.assertEquals(reminder.repeatFrequency, 2);
    }

    /**
     * "remind me every saturday morning to take Eve to tennis"
     *          |--> "remind me every saturday  at 6 am to take Eve to tennis"
     *                      |--> "remind me Saturdays at 9:30 to take Eve to tennis"
     */
    @org.testng.annotations.Test
    public void test_recurring_every_dayOfWeek() {
        String phrase = "remind me every Saturday morning to take Eve to tennis";

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "take eve to tennis");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE); //no call to action ==> no FUTURE status

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.SATURDAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }


    /**
     * every Saturday in the evening
     * every Saturday at 9 in the evening
     * every Saturday at 9 pm
     *                  |____________--> Saturdays at 9 pm
     */

    @org.testng.annotations.Test
    public void test_recurring_every_DayOfWeek_2() {
        String phrase = "remind me to take the trash out to the curb every Thursday in the evening";

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.THURSDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "take the trash out to the curb");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE); //no call to action ==> no FUTURE status

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.THURSDAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_recurring_every_DayOfWeek_2b() {
        String phrase = "remind me to take the trash out to the curb in the evening every Thursday"; //alt. test: "... on Thursday"

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.THURSDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "take the trash out to the curb");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE); //no call to action ==> no FUTURE status

        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.THURSDAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_dayOfWeek_complexTimeOfDay_timeAsSeparateNums() {
        String phrase = "i will call you Thursday in the morning around 8 10 am to talk about the new initiative";

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.THURSDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);

        expected.set(Calendar.HOUR_OF_DAY, 8);
        expected.set(Calendar.MINUTE, 10);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.EVENT_DETECTED);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "i will call you to talk about the new initiative");
        Assert.assertEquals(reminder.recurring, false);
    }

    @org.testng.annotations.Test
    public void test_dayOfWeek_vagueTimeOfDay() {
        String phrase = "remind me on Thursday to do something in the evening";

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.THURSDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.ASK_FOR_TIME.name()));
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.recurring, false);
    }

    /**
     * "every other" wasn't picked up
     */
    @org.testng.annotations.Test
    public void test_recurring_everyOther_dayOfWeek_vagueTimeOfDay() {
        String phrase = "remind me every other Thursday to take the garbage out to the curb in the evening";

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.THURSDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.ASK_FOR_TIME.name()));
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "take the garbage out to the curb");
        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.THURSDAYS);
        Assert.assertEquals(reminder.repeatFrequency, 2);
    }

    /**
     * testing "every saturday" in the beginning of a sentence
     */
    @org.testng.annotations.Test
    public void test_recurring_every_atTheBeginning() {
        String phrase = "every Saturday at 9 in the evening";

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);

        expected.set(Calendar.HOUR_OF_DAY, 21);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
        Assert.assertTrue(reminder.hint.getAll().contains(Reminder.Hint.USE_CALL_TO_ACTION.name()));
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "");
        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.SATURDAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    /**
     * test vague date specification, i.e. next moth
     */
    @org.testng.annotations.Test
    public void test_vague_dateSpecified_nextMonth() {
        String phrase = "we will fly a kite next month";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.MONTH, 1);
        expected.set(Calendar.DATE, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "we will fly a kite");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
        Assert.assertEquals(reminder.recurring, false);
    }

    @org.testng.annotations.Test
    public void test_date_nextYear() {
        String phrase = "let's leave on Apr. 6 next year";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.YEAR, 1);
        expected.set(Calendar.MONTH, Calendar.APRIL);
        expected.set(Calendar.DATE, 6);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "let's leave");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.NO_REMINDER_DETECTED);
        Assert.assertEquals(reminder.recurring, false);
    }


    @org.testng.annotations.Test
    public void test_date_nextMonthByName() {
        String phrase = "remind me about Mary's birthday next August";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.YEAR, 1);
        expected.set(Calendar.MONTH, Calendar.AUGUST);
        expected.set(Calendar.DATE, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "mary's birthday");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.recurring, false);
    }

    @org.testng.annotations.Test
    public void test_date_thisMonthByName() {
        String phrase = "remind me about Mary's birthday this August";
        Calendar now = Calendar.getInstance();

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.MONTH, Calendar.AUGUST);
        expected.set(Calendar.DATE, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "mary's birthday");
        if (expected.after(now))
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        else
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.LAPSED);
        Assert.assertEquals(reminder.recurring, false);
    }

    @org.testng.annotations.Test
    public void test_date_everyMonthByName() {
        String phrase = "remind me about Mary's birthday every August";

        Calendar now = Calendar.getInstance();
        Calendar expected = Calendar.getInstance();
        if (now.get(Calendar.MONTH)>Calendar.AUGUST)
            expected.add(Calendar.YEAR, 1);
        expected.set(Calendar.MONTH, Calendar.AUGUST);
        expected.set(Calendar.DATE, 1);
        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "mary's birthday");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.AUGUST);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_date_everyYear_SpecificDayGiven() {
        String phrase = "remind me about Mary's birthday every year on August 10th";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.MONTH)>=Calendar.AUGUST)
            expected.add(Calendar.YEAR, 1);
        expected.set(Calendar.MONTH, Calendar.AUGUST);
        expected.set(Calendar.DATE, 10);

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "mary's birthday");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.YEARS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_date_halfAnHourAdv_SpecificTimeGiven() {
        String phrase = "remind me half an hour in advance that this needs to go out just before eight today";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.HOUR_OF_DAY) < 8)
            expected.set(Calendar.HOUR_OF_DAY, 8);
        else
            expected.set(Calendar.HOUR_OF_DAY, 20);
        expected.set(Calendar.MINUTE, 0);
        expected.add(Calendar.MINUTE, AmbiguousTime.SHIFT_JUST_BEFORE);

        //"half hour in advance"
        expected.add(Calendar.MINUTE, -30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "this needs to go out");
        if (now.after(expected))
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.LAPSED);
        else
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.recurring, false);
    }

    // "remind me 30 minutes in advance that this needs to go out just before eight today" - FAIL - does not ffwd to the next am/pm, status resolved to LAPSED, specifying "tonight" correctly set the time


    @org.testng.annotations.Test
    public void test_date_halfAnHourAdv_AmPmSpecifiedByTimeOfDay() {
        String phrase = "Wake me up half an hour before the trip tomorrow afternoon at 4 in the afternoon";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.DATE, 1);
        expected.set(Calendar.HOUR_OF_DAY, 16);
        expected.set(Calendar.MINUTE, 0);
        expected.add(Calendar.MINUTE, -30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, null);
        Assert.assertEquals(reminder.text, "wake me up the trip");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.EVENT_DETECTED); //only *reminders have* FUTURE or LAPSED
        Assert.assertEquals(reminder.recurring, false);
    }

    @org.testng.annotations.Test
    public void test_date_recurring_useOfOnceWord() {
        String phrase = "remind me to get up and go for a walk once an hour";

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.HOUR_OF_DAY, 1);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "get up and go for a walk");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.HOURS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    @org.testng.annotations.Test
    public void test_outOfOrder_Action() {
        String phrase = "this needs to go out right after six, remind me ten minutes early";

        Calendar expected = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.HOUR_OF_DAY)>=18) { // || (now.get(Calendar.HOUR_OF_DAY)==18 && now.get(Calendar.MINUTE) > 0))
            expected.add(Calendar.DATE, 1);
            expected.set(Calendar.HOUR_OF_DAY, 6);
        } else if (now.get(Calendar.HOUR_OF_DAY)>=6) {
            expected.set(Calendar.HOUR_OF_DAY, 18);
        } else
            expected.set(Calendar.HOUR_OF_DAY, 6);

        expected.set(Calendar.MINUTE, 0);
        expected.add(Calendar.MINUTE, AmbiguousTime.SHIFT_JUST_AFTER);
        expected.add(Calendar.MINUTE, -10);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "this needs to go out");
        Assert.assertEquals(reminder.recurring, false);
    }


    // "get ready tonight at 7pm for tomorrow" - FAIL, picks up tomorrow

    //"remind me to make the monthly call each month on the 15th day of the month"

    //"remind me to make the monthly call each month on the 15th day of January"

    //"remind me to make the monthly call each month on the 15th"

    //"remind me to do something on <previous-month> 15th at 8:30 am" --> should come beack LAPSED
    //"remind me to do something on <previous-month> 15th next year at 8:30 am" --> should come back FUTURE
    //"remind me to do something on <previous-month> 15th of next year at 8:30 am" --> use of "of next year" should have no effect, result same as above

    @org.testng.annotations.Test
    public void test_date_LAPSED_Month() {

        Calendar testdate = Calendar.getInstance();
        testdate.add(Calendar.MONTH, -1);
        String testMonth = CalendarUtils.getMonthAsString(testdate.get(Calendar.MONTH));

        String phrase = "remind me to do something on "+testMonth+" 15th at 8:30 am";

        Calendar expected = Calendar.getInstance();
        expected.set(Calendar.MONTH, testdate.get(Calendar.MONTH));
        expected.set(Calendar.DATE, 15);
        expected.set(Calendar.HOUR_OF_DAY, 8);
        expected.set(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        /*
            Year boundary crossing is a special edge case because in Jan, month - 1 evaluates to Dec,
            which still happens to be FUTURE in Jan of the following year. Any other testmonth should evaluate to LAPSED.
         */
        if (testdate.get(Calendar.MONTH)==12)
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        else
            Assert.assertEquals(reminder.status, Reminder.ReminderStatus.LAPSED);

        Assert.assertEquals(reminder.recurring, false);
    }

    @org.testng.annotations.Test
    public void test_date_LAPSED_Month_nextYear() {
        Calendar testdate = Calendar.getInstance();
        testdate.add(Calendar.MONTH, -1);
        String testMonth = CalendarUtils.getMonthAsString(testdate.get(Calendar.MONTH));

        String phrase = "remind me to do something on "+testMonth+" 15th next year at 8:30 am";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.YEAR, 1);
        expected.set(Calendar.MONTH, testdate.get(Calendar.MONTH));
        expected.set(Calendar.DATE, 15);
        expected.set(Calendar.HOUR_OF_DAY, 8);
        expected.set(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.recurring, false);
    }

    @org.testng.annotations.Test
    public void test_date_LAPSED_Month_nextYear_withOf() {
        Calendar testdate = Calendar.getInstance();
        testdate.add(Calendar.MONTH, -1);
        String testMonth = CalendarUtils.getMonthAsString(testdate.get(Calendar.MONTH));

        String phrase = "remind me to do something on "+testMonth+" 15th of next year at 8:30 am";

        Calendar expected = Calendar.getInstance();
        expected.add(Calendar.YEAR, 1);
        expected.set(Calendar.MONTH, testdate.get(Calendar.MONTH));
        expected.set(Calendar.DATE, 15);
        expected.set(Calendar.HOUR_OF_DAY, 8);
        expected.set(Calendar.MINUTE, 30);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "do something");
        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.recurring, false);
    }

    /**
     * Test use of word "each" twice in the phrase
     */
    @org.testng.annotations.Test
    public void test_recurring_doubleRepeatSpecified() {
        String phrase = "remind me to start each week well each Monday at 10 am";

        Calendar expected = Calendar.getInstance();

        expected.add(Calendar.DAY_OF_WEEK, 1);
        while (expected.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
            expected.add(Calendar.DAY_OF_WEEK, 1);

        expected.set(Calendar.HOUR_OF_DAY, 10);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertEquals(reminder.status, Reminder.ReminderStatus.FUTURE);
        Assert.assertEquals(reminder.callToAction, "remind me");
        Assert.assertEquals(reminder.text, "start well");
        Assert.assertEquals(reminder.recurring, true);
        Assert.assertEquals(reminder.repeatTarget, RecurringParser.RecurringTarget.MONDAYS);
        Assert.assertEquals(reminder.repeatFrequency, 1);
    }

    /**
     * Test use of word "each" twice in the phrase
     */
    @org.testng.annotations.Test
    public void test_doubleTimeOfDay() {
        String phrase = "get ready tonight for tomorrow";

        Calendar expected = Calendar.getInstance();

        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.TONIGHT);
        expected.set(Calendar.MINUTE, 0);

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(phrase);

        verifyDateTimeResult(reminder.date, expected);

        Assert.assertNull(reminder.callToAction);
        Assert.assertEquals(reminder.text, "get ready for tomorrow");
        Assert.assertEquals(reminder.recurring, false);
    }

    // "Valentine's Day this Sunday, remind me on Thursday" - FAIL: picks Sunday over Thursday

    // remind me 6 days early about mary's b-day every 2 years on august 15th

    // get ready tonight for tomorrow


    // "enter a reminder text on jan 15" - ?FAIL? not sure, resolves to the mond-day of the current year, specifying "next monh" or "next year" is a PASS

    // "remind me to do this and that later tonight"

    // "remind me 2 hours early about the shopping trip tonight at 11"

    // "Wake me up 30 minutes early before the trip tonight at seven forty five" - PASS

    // "we will be done in eight min" - PASS

    // "March 15 2016 - be there!" - PASS

    //"Remind me to feed the crickets on October 7 2016 at 10:05 am" - FAIL: the time stays on 10:00 am, minutes are ignored (bad regex?)

    // "remind me to do something before midnight" - EDGE CASE FAIL: marked as LAPSED if the time is between 23:45 and 23:59, "before midnight" normalizes to 23:45 so anything after that in that 15 minute period is wrongfully marked as LAPSED

    // "remind me the day before that I said I will send it within next 2 weeks" - PASS: expected add two weeks and subtract a day

    /* TODO: tests and use cases

        //   Test: remind me to do something at 425
        //   Test: remind me to do something at 10

        [] if reminders are set before 3 or 4 am and set the day to "tomorrow" then use "today" as current time
            i.e. it is 1 am: "remind me to do something tomorrow at lunch" most likely means at noon of that day "today at lunch"

        [] NICE-TO-HAVE: detect when a user speaks about 'self' and replace all "my"'s with "your"
                i.e. "remind me in an hour to keep my cool" , text field should resolve to "keep your cool"

        TODO: sessions: every time a non-specific time is provided ("in the morning", "at night")
        session should ask back with a suggested time and offer user opportunity to provide another time

     */

    public void verifyDateTimeResult(Calendar current, Calendar expected)
    {
        Assert.assertEquals(current.get(Calendar.YEAR)   , expected.get(Calendar.YEAR));
        Assert.assertEquals(current.get(Calendar.MONTH)  , expected.get(Calendar.MONTH));
        Assert.assertEquals(current.get(Calendar.DAY_OF_MONTH)   , expected.get(Calendar.DAY_OF_MONTH));
        Assert.assertEquals(current.get(Calendar.HOUR_OF_DAY)   , expected.get(Calendar.HOUR_OF_DAY));
        Assert.assertEquals(current.get(Calendar.MINUTE) , expected.get(Calendar.MINUTE));
    }

    public Calendar expectNextTimeOfDay(Calendar expected) {
        Calendar now = Calendar.getInstance();
        Calendar next = Calendar.getInstance();
        next.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NOON);
        if (now.before(next)) {
            expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NOON);
            expected.set(Calendar.MINUTE,0);
        } else {
            next.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTER_LUNCH);
            if (now.before(next)) {
                expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTER_LUNCH);
                expected.set(Calendar.MINUTE,0);
            } else {
                next.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
                if (now.before(next)) {
                    expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
                    expected.set(Calendar.MINUTE,0);
                } else {
                    next.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
                    if (now.before(next)) {
                        expected.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
                        expected.set(Calendar.MINUTE,0);
                    } else {
                    }
                }
            }
        }
        return expected;
    }

}
