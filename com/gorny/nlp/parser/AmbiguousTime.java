package com.gorny.nlp.parser;

public class AmbiguousTime
{
    public static int MORNING           = 6;
    public static int BEFORE_LUNCH      = 11;
    public static int NOON              = 12;
    public static int AFTER_LUNCH       = 14;
    public static int AFTERNOON         = 15;
    public static int EVENING           = 18;
    public static int TONIGHT           = 20;
    public static int BEFORE_MIDNIGHT_HR = 0;
    public static int BEFORE_MIDNIGHT_MN = 0;
    public static int MIDNIGHT          = 0;
    public static int AFTER_MIDNIGHT_HR = 0;
    public static int AFTER_MIDNIGHT_MN = 10;
    public static int NIGHT             = 21;
    public static int COUPLE_OF         = 2;
    public static int FEW               = 4;
    public static int SEVERAL           = 7;
    public static int LATER             = 90;

    public static int SHIFT_BY          = -30;
    public static int SHIFT_BEFORE      = -15;
    public static int SHIFT_JUST_BEFORE = -4;
    public static int SHIFT_AFTER       =  5;
    public static int SHIFT_JUST_AFTER  =  3;

    public static int NON_ZERO_MINUTES  = 5;
    public static int END_OF_DAY_HOUR 	= 16;
    public static int END_OF_DAY_MINS 	= 45;
}
