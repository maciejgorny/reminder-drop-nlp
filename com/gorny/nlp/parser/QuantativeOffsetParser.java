package com.gorny.nlp.parser;

import com.gorny.nlp.Reminder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parser handles offset specified as quantity of time
 * i.e. "10 minutes before", or "2 days in advance"
 */
public class QuantativeOffsetParser {

    public static boolean parse(Reminder reminder, String input)
    {
        String text = input.trim();
        System.out.println("QuantativeOffsetParser calc offset from \""+text+"\"");

        String shift = null;
        int coefficient = 0;
        int minute_multiplier = 0; //in minutes, because a minute is the lowest resolution in a time slice
        int quantity = 0;

        /*extract quantity*/

        Pattern p = Pattern.compile("\\d+", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        if (m.find()) {
            String sn = m.group();
            try { quantity = Integer.parseInt(sn); }
            catch(NumberFormatException e) {}
        }

        /*determine minutes multiplier*/

        p = Pattern.compile("\\s+(year|month|week|day|hour|minute|min|min.)s*", Pattern.CASE_INSENSITIVE);
        m = p.matcher(text);
        if (m.find()) {
            String sm = m.group().toUpperCase().trim();
            if (sm.startsWith("YEAR"))
                minute_multiplier = 60 * 24 * 365;
            if (sm.startsWith("MONTH"))
                minute_multiplier = 60 * 24 * 30;
            if (sm.startsWith("DAY"))
                minute_multiplier = 60 * 24;
            if (sm.startsWith("HOUR"))
                minute_multiplier = 60;
            if (sm.startsWith("MIN"))
                minute_multiplier = 1;
        }

        /*before pattern*/

        p = Pattern.compile("\\s(before|ahead|early|earlier|in advance|ahead of time)", Pattern.CASE_INSENSITIVE);
        m = p.matcher(text);
        if (m.find()) {
            shift = m.group().toUpperCase();
            coefficient = -1;
        }

        /*after pattern*/

        p = Pattern.compile("\\s(after|past|pass|passed|later)\\s+", Pattern.CASE_INSENSITIVE);
        m = p.matcher(text);
        if (m.find()) {
            shift = m.group().toUpperCase();
            coefficient = 1;
        }

        if (coefficient != 0 && minute_multiplier > 0 && quantity > 0) {

            if (reminder.finalOffset == null)
                reminder.finalOffset = reminder.new FinalOffset();

            reminder.finalOffset.minutesDiff = coefficient * quantity * minute_multiplier;
        }

        return false;
    }
}
