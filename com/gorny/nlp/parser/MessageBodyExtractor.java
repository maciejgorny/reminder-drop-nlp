package com.gorny.nlp.parser;

import com.gorny.nlp.Reminder;

public class MessageBodyExtractor {

    public static String AFTER_ABOUT_THAT_TO = "((?=\\sabout\\s)(.*?)(?=[.\\?;]|$))";

    public static boolean parse(Reminder reminder, String text) {
        String result = text.trim();
        boolean found = false;
        if (result.startsWith("about")) {
            result = result.substring(6);
            found = true;
        }
        else if (result.startsWith("to")) {
            result = result.substring(3);
            found = true;
        }
        else if (result.startsWith("that")) {
            result = result.substring(5);
            found = true;
        }
        reminder.text = result;
        return found;
    }
}