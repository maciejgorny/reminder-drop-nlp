package com.gorny.nlp.parser;

import com.gorny.nlp.Reminder;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MonthOfYearParser {

    public static String MONTHS = "(january|february|march|april|may|june|july|august|september|october|november|december)";
    public static String EXPRESSION = "(in|this|next|every|each)?\\s+"+MONTHS+"(\\s+|$)";

    public static boolean parse(Reminder reminder, String input)
    {
        Pattern p = Pattern.compile(MONTHS, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(input);
        if (m.find()) {
            String month = m.group().toUpperCase();
            int set = -1;
            if (month.startsWith("JA")) {
                set = Calendar.JANUARY;
            } else if (month.startsWith("FE")) {
                set = Calendar.FEBRUARY;
            } else if (month.startsWith("MAR")) {
                set = Calendar.MARCH;
            } else if (month.startsWith("AP")) {
                set = Calendar.APRIL;
            } else if (month.startsWith("MA")) {
                set = Calendar.MAY;
            } else if (month.startsWith("JUN")) {
                set = Calendar.JUNE;
            } else if (month.startsWith("JU")) {
                set = Calendar.JULY;
            } else if (month.startsWith("AU")) {
                set = Calendar.AUGUST;
            } else if (month.startsWith("SE")) {
                set = Calendar.SEPTEMBER;
            } else if (month.startsWith("OC")) {
                set = Calendar.OCTOBER;
            } else if (month.startsWith("NO")) {
                set = Calendar.NOVEMBER;
            } else if (month.startsWith("DE")) {
                set = Calendar.DECEMBER;
            }
            if (set >= 0) {
                reminder.date.set(Calendar.MONTH, set);
                reminder.date.set(Calendar.DATE, 1);
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
                reminder.date.set(Calendar.MINUTE, 0);

                Calendar now = Calendar.getInstance();
                if (now.get(Calendar.MONTH) >= set) {
                    //do not advance if we already are set for the following year
                    if (now.get(Calendar.YEAR)==reminder.date.get(Calendar.YEAR))
                        reminder.date.add(Calendar.YEAR, 1);
                }
                int nextLoopCount = input.split("next").length - 1;
                while (nextLoopCount > 0) {
                    reminder.date.add(Calendar.YEAR, 1);
                    nextLoopCount --;
                }
            }
            /* Technically this pattern and regex should be in RecurringParser,
             * but recurring parser is becoming overly complex and this one here
             * is still simple to manage and maintain.
             */
            if (input.startsWith("every")||input.startsWith("each")) {
                reminder.recurring = true;
                reminder.repeatTarget = RecurringParser.getRepeatTarget(month);
                reminder.repeatFrequency = 1;
            }
        }
        reminder.inflate();
        return true;
    }

}
