package com.gorny.nlp.parser;

import com.gorny.nlp.CalendarUtils;
import com.gorny.nlp.NamedEntities;
import com.gorny.nlp.Reminder;

import java.util.Calendar;

public class DayOfWeekParser
{
    public static String EXPRESSION = "(on|this|next)?\\s+(Mon|mon|Tues|tues|Wednes|wednes|Thurs|thurs|Fri|fri|Satur|satur|Sun|sun)day";//(\\s+|$)";

    //TODO: extend this expr to give preference on matches following a call to action: i.e. "something is happening this Sunday, remind me on Thursday"
    //This should be extended to all expressions, as "something to do in August, remind me in July" is just as valid
    //(((remember|reminder|tell|remind)\s+(me|us|\S+)|(don't|do not)\s+forget)\s+(to|that|about)?)\K([\w\s]*|(on|this|next)?\s+(Mon|mon|Tues|tues|Wednes|wednes|Thurs|thurs|Fri|fri|Satur|satur|Sun|sun)day)

    //actions
    //((remember|reminder|tell|remind)\s+(me|us|\S+)|(don't|do not)\s+forget)\s+(to|that|about)?
    //remind me to A, remember to B, tell us that C, reminder that D, don't forget that E, do not forget to F, remind us  about G
    //
    public static String EXPR_WITH_CALL_TO_ACTION = "("+NamedEntities.CALL_TO_ACTION.All.expression()+")\\K([\\w\\s]*|"+EXPRESSION+")";

    public static boolean parse(Reminder reminder, String input)
    {
        String text = input.toUpperCase().substring(input.indexOf(" ") + 1);
        System.out.println("EXPR_WITH_CALL_TO_ACTION: "+EXPR_WITH_CALL_TO_ACTION);

        int nextLoopCount = input.split("next").length - 1;
        boolean resetTime = false;
        while (nextLoopCount >= 0) {
        /* starting with tomorrow ...*/
            reminder.date.add(Calendar.DAY_OF_MONTH, 1);

        /* go a day at a time until we find a match */
            while (reminder.date.get(Calendar.DAY_OF_WEEK) != CalendarUtils.DAYOFWEEK.valueOf(text).number()) {
                reminder.date.add(Calendar.DAY_OF_MONTH, 1);
                resetTime = true;
            }
            nextLoopCount --;
        }

        resetTime = resetTime || CalendarUtils.isTomorrowOrLater(reminder.date);

        if (resetTime && !reminder.haveTime) {
            reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
            reminder.date.set(Calendar.MINUTE, 0);
        }
        reminder.inflate();
        return true;
    }

}
