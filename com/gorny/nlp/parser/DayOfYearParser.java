package com.gorny.nlp.parser;

import com.gorny.nlp.CalendarUtils;
import com.gorny.nlp.Reminder;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DayOfYearParser
{
    public static boolean parse(Reminder reminder, String text)
    {
        Pattern p = Pattern.compile("january|february|march|april|may|june|july|august|september|october|november|december" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        boolean found = m.find();
        if (found)
        {
            String monthStr = m.group().toUpperCase();
            int monthEnum = CalendarUtils.MONTH.valueOf(monthStr).number();

            reminder.date.set(Calendar.MONTH, monthEnum);

            p = Pattern.compile("\\d+(th)?");
            m = p.matcher(text);
            if (m.find())
            {
                String day = m.group();
                int dayNo = Integer.parseInt(CalendarUtils.NUMBER(day));
                reminder.date.set(Calendar.DAY_OF_MONTH, dayNo);
                reminder.haveDay = true;
            }

            p = Pattern.compile("20[\\d]+");
            m = p.matcher(text);
            if (m.find())
            {
                String year = m.group();
                int yearNo = Integer.parseInt(CalendarUtils.NUMBER(year));
                reminder.date.set(Calendar.YEAR, yearNo);
            }

            Calendar tomorrow = Calendar.getInstance();
            tomorrow.add(Calendar.DATE, 1);

            if (CalendarUtils.isTomorrowOrLater(reminder.date)) {
                reminder.date.set(Calendar.HOUR_OF_DAY, 6);
                reminder.date.set(Calendar.MINUTE, 0);
            }
        }
        // This could be a separate DayOfMonth parser
        // Picks up "on the 20th of" etc...
        p = Pattern.compile("on(\\s+the)*\\s+(\\d{1,2}(nd|rd|th))(\\s+of)*" , Pattern.CASE_INSENSITIVE);
        m = p.matcher(text);
        found = m.find();
        if (found) {
            String group = m.group();
            p = Pattern.compile("\\d{1,2}");
            m = p.matcher(group);
            if (m.find()) {
                Integer dayOfMonth = Integer.parseInt(m.group());
                reminder.date.set(Calendar.DATE, dayOfMonth);
                if (CalendarUtils.isTomorrowOrLater(reminder.date)) {
                    reminder.date.set(Calendar.HOUR_OF_DAY, 6);
                    reminder.date.set(Calendar.MINUTE, 0);
                }
            }
        }
        reminder.inflate();
        return reminder.haveDay;
    }

}
