package com.gorny.nlp.parser;

import com.gorny.nlp.CalendarUtils;
import com.gorny.nlp.Reminder;

import java.util.Calendar;

public class AmbiguousTimeParser
{
    public static String EXPRESSION = "(this morning|in the morning|morning|at noon|this afternoon|in the afternoon|afternoon|before lunch|by lunch|at lunch time|at lunch|during lunch|after lunch|this evening|in the evening|evening|at night|night|midnight|by midnight|before midnight|after midnight|tonight|later)";

    public static boolean parse(Reminder reminder, String text)
    {
        CalendarUtils.AMBIGUOUS_DAY_TIME field;
        try {
          field = CalendarUtils.AMBIGUOUS_DAY_TIME.valueOf(text.toUpperCase().replaceAll(" ", "_"));
        } catch (IllegalArgumentException e) {
            return false;
        }
        switch(field)
        {
            case MORNING:
            case IN_THE_MORNING:
            case THIS_MORNING:
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
                reminder.ampm = Calendar.AM;
                reminder.haveAmPm = true;
                break;

            case AFTERNOON:
            case IN_THE_AFTERNOON:
            case THIS_AFTERNOON:
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTERNOON);
                reminder.ampm = Calendar.PM;
                reminder.haveAmPm = true;
                break;

            case TONIGHT:
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.TONIGHT);
                reminder.ampm = Calendar.PM;
                reminder.haveDay = true;
                reminder.haveAmPm = true;
                break;

            case THIS_EVENING:
            case IN_THE_EVENING:
                
            case EVENING:
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
                reminder.ampm = Calendar.PM;
                reminder.haveAmPm = true;
                break;

            case AT_NIGHT:
                reminder.haveDay = true;
            case NIGHT:
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
                reminder.ampm = Calendar.PM; //"Call Jim Wed. night at 8" --> P.M.
                reminder.haveAmPm = true;
                break;

            case BEFORE_LUNCH:
            case BY_LUNCH:
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.BEFORE_LUNCH);
                reminder.ampm = Calendar.AM;
                reminder.haveAmPm = true;
                break;

            case AT_NOON:
            case AT_LUNCH:
            case AT_LUNCH_TIME:
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NOON);
                reminder.ampm = Calendar.PM;
                reminder.haveAmPm = true;
                break;

            case AFTER_LUNCH:
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTER_LUNCH);
                reminder.ampm = Calendar.PM;
                reminder.haveAmPm = true;
                break;

            case MIDNIGHT:
                reminder.date.add(Calendar.DAY_OF_WEEK, 1);
                reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MIDNIGHT);
                reminder.date.set(Calendar.MINUTE, 0);
                reminder.ampm = Calendar.PM;
                break;

            case LATER:
                reminder.date.add(Calendar.MINUTE, AmbiguousTime.LATER);
                break;
        }
        reminder.date.set(Calendar.MINUTE, 0);
        reminder.hint = Reminder.Hint.ASK_FOR_TIME;
        reminder.inflate();

        return true;
    }

}
