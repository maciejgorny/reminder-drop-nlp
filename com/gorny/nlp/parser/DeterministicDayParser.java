package com.gorny.nlp.parser;

import com.gorny.nlp.CalendarUtils;
import com.gorny.nlp.Reminder;

import java.util.Calendar;

public class DeterministicDayParser extends BaseParser
{
    public static String EXPRESSION = "(?<!for|about)\\s+(tomorrow[!:\\.;,\\s]|tomorrow$|after tomorrow(\\s?)|after tomorrow$|today(\\s?)|today$)";

    public static boolean parse(Reminder reminder, String text)
    {
        CalendarUtils.AMBIGUOUS_DAY_TIME field = CalendarUtils.AMBIGUOUS_DAY_TIME.valueOf(text.trim().toUpperCase().replaceAll(" ", "_"));
        switch(field) {

            case AFTER_TOMORROW:
                reminder.date.add(Calendar.DAY_OF_MONTH, 2);
                break;

            case TOMORROW:
                reminder.date.add(Calendar.DAY_OF_MONTH, 1);
                break;

            case TODAY:
                reminder.date.add(Calendar.HOUR_OF_DAY, 1);
                break;
        }
        reminder.inflate();
        return true;
    }
}
