package com.gorny.nlp.parser;

import com.gorny.nlp.CalendarUtils;
import com.gorny.nlp.Reminder;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeShiftParser
{
    public static boolean parse(Reminder reminder, String text)
    {
        Pattern p = Pattern.compile("second|minute|hour|day|week|month|year", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);

        boolean found = m.find();
        if (found)
        {
            String timeUnit = m.group().toUpperCase();
            CalendarUtils.TIME_UNIT time = CalendarUtils.TIME_UNIT.valueOf(timeUnit);

            p = Pattern.compile("\\d+");
            m = p.matcher(text);
            if (m.find())
            {
                String N = m.group();
                int amount = Integer.parseInt(N);

                reminder.date.add(time.unit(), amount);
                if (time != CalendarUtils.TIME_UNIT.HOUR)
                    CalendarUtils.resetTimeIfFutureAndAmbiguous(reminder);
            }
        }
        reminder.inflate();
        return found;
    }
}