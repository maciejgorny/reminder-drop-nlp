package com.gorny.nlp.parser;

import com.gorny.nlp.CalendarUtils;
import com.gorny.nlp.Reminder;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecurringParser {

    public static String DAYS           = "(\\s+|^)(Mon|mon|Tues|tues|Wednes|wednes|Thurs|thurs|Fri|fri|Satur|satur|Sun|sun)days";//(\\s+|$)";
    public static String N_OF           = "(on(\\s+the)*\\s+(\\d{1,2}(nd|rd|th)*(\\s+day)*\\s+of\\s+)*)";
    public static String OF_N           = "(\\s+on(\\s+the)*\\s+\\d{1,2}(st|nd|rd|th)*(\\s+of)(\\s+(the|a)*)*(\\s+month))";
    public static String EVERY_N        = "(every|each|once(\\san)*)\\s+((a|\\d+(nd|rd|th)*)\\s+)*((minute|hour|day|week|weekday|weekend|month|year|((Mon|mon|Tues|tues|Wednes|wednes|Thurs|thurs|Fri|fri|Satur|satur|Sun|sun)day))(s*))";
    public static String EVERY_N_REPEAT = "(on(\\s+the)*\\s+(\\d{1,2}(nd|rd|th)*(\\s+day)*\\s+of\\s+)*)*(every|each|once(\\san)*)\\s+((a|\\d+(nd|rd|th)*)\\s+)*((minute|hour|day|week|weekday|weekend|month|year|((Mon|mon|Tues|tues|Wednes|wednes|Thurs|thurs|Fri|fri|Satur|satur|Sun|sun)day))(s*))"+OF_N+"*";


    public enum RecurringTarget {
        NONE,
        MINUTES,
        HOURS,
        DAYS,
        WEEKS,
        WEEKDAYS,
        WEEKEND_DAYS,
        MONDAYS, TUESDAYS, WEDNESDAYS, THURSDAYS, FRIDAYS, SATURDAYS, SUNDAYS,
        MONTHS,
        YEARS,
        JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER,
        DATE_TIME
    }

    /**
     * The purpose of the RecurringParser is not to alter the date object already built by now,
     * but to mark up the Reminder object with a repeat structure that will indicate (or allow to conclude)
     * the next date and time of a recurring reminder.... - nah we're altering the date object.
     */
    public static boolean parse(Reminder reminder, String text)
    {
        Calendar now = Calendar.getInstance();

        Pattern p = Pattern.compile(EVERY_N_REPEAT, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);

        boolean found = m.find();
        if (found && reminder.repeatTarget == RecurringTarget.NONE)
        {
            String extracted = text.substring(m.start(), m.end());
            reminder.recurring = true;
            reminder.repeatTarget = getRepeatTarget(extracted);

            p = Pattern.compile(N_OF, Pattern.CASE_INSENSITIVE);
            m = p.matcher(text);
            if (m.find()) {
                //i.e. "on the 20th of"
                String group = m.group();
                p = Pattern.compile("\\d{1,2}");
                m = p.matcher(group);
                if (m.find()) {
                    group = m.group();
                    Integer day = Integer.parseInt(group);
                    reminder.date.set(Calendar.DAY_OF_MONTH, day);
                    if (!reminder.haveTime) {
                        reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
                        reminder.date.set(Calendar.MINUTE, 0);
                    }
                    reminder.haveDay = true;
                }
            }

            //just see if there is a every N pattern
            p = Pattern.compile(EVERY_N, Pattern.CASE_INSENSITIVE);
            m = p.matcher(text);
            if (m.find()) {
                //i.e. "every N minutes|hours|days ..."
                String group = m.group();
                p = Pattern.compile("\\s\\d+");
                m = p.matcher(group);
                if (m.find()) {
                    group = m.group().trim();
                    reminder.repeatFrequency = Integer.parseInt(group);
                    switch (reminder.repeatTarget) {
                        case MINUTES:
                            reminder.date.add(Calendar.MINUTE, reminder.repeatFrequency);
                            break;
                        case HOURS:
                            reminder.date.add(Calendar.HOUR, reminder.repeatFrequency);
                            break;
                        case DAYS:
                            if (!reminder.haveTime && reminder.date.after(now))
                                reminder.date.add(Calendar.DATE, reminder.repeatFrequency);
                            break;
                        case WEEKS:
                            reminder.date.add(Calendar.DATE, 7 * reminder.repeatFrequency);
                            break;
                        case WEEKDAYS:
                            break;
                        case WEEKEND_DAYS:
                            break;
                        case MONTHS:
                            reminder.date.add(Calendar.MONTH, reminder.repeatFrequency);
                            break;
                        case YEARS:
                            if (now.after(reminder.date))
                                reminder.date.add(Calendar.YEAR, reminder.repeatFrequency);
                            break;
                        case MONDAYS:
                            CalendarUtils.setNextEventForRecurringReminder(reminder);
                            break;
                        case TUESDAYS:
                            CalendarUtils.setNextEventForRecurringReminder(reminder);
                            break;
                        case WEDNESDAYS:
                            CalendarUtils.setNextEventForRecurringReminder(reminder);
                            break;
                        case THURSDAYS:
                            CalendarUtils.setNextEventForRecurringReminder(reminder);
                            break;
                        case FRIDAYS:
                            CalendarUtils.setNextEventForRecurringReminder(reminder);
                            break;
                        case SATURDAYS:
                            CalendarUtils.setNextEventForRecurringReminder(reminder);
                            break;
                        case SUNDAYS:
                            CalendarUtils.setNextEventForRecurringReminder(reminder);
                            break;
                    }
                    reminder.haveTime = true;
                }
            }

            p = Pattern.compile(OF_N, Pattern.CASE_INSENSITIVE);
            m = p.matcher(text);
            if (m.find()) {
                //i.e. "on the 1st of the month"
                String group = m.group();
                p = Pattern.compile("\\d{1,2}");
                m = p.matcher(group);
                if (m.find()) {
                    group = m.group();
                    Integer day = Integer.parseInt(group);
                    reminder.date.set(Calendar.DAY_OF_MONTH, day);
                    reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
                    reminder.date.set(Calendar.MINUTE, 0);
                    reminder.haveDay = true;
                }
            }

        }
        reminder.inflate();
        return found;
    }

    public static RecurringTarget getRepeatTarget(String entity)
    {
        if (entity.endsWith("minute")||entity.endsWith("minutes"))
            return RecurringTarget.MINUTES;

        else if (entity.endsWith("hour")||entity.endsWith("hours"))
            return RecurringTarget.HOURS;

        else if (entity.endsWith("week")||entity.endsWith("weeks"))
            return RecurringTarget.WEEKS;

        else if (entity.endsWith("month")||entity.endsWith("months"))
            return RecurringTarget.MONTHS;

        else if (entity.endsWith("year")||entity.endsWith("years"))
            return RecurringTarget.YEARS;

        else if (entity.endsWith("monday"))
            return RecurringTarget.MONDAYS;

        else if (entity.endsWith("tuesday"))
            return RecurringTarget.TUESDAYS;

        else if (entity.endsWith("wednesday"))
            return RecurringTarget.WEDNESDAYS;

        else if (entity.endsWith("thursday"))
            return RecurringTarget.THURSDAYS;

        else if (entity.endsWith("friday"))
            return RecurringTarget.FRIDAYS;

        else if (entity.endsWith("saturday"))
            return RecurringTarget.SATURDAYS;

        else if (entity.endsWith("sunday"))
            return RecurringTarget.SUNDAYS;

        else if (entity.endsWith("day")||entity.endsWith("days"))
            return RecurringTarget.DAYS;

        /*
         * all else case (covers month names)
         */
        try {
            RecurringTarget target = RecurringTarget.valueOf(entity);
            return target;
        } catch (Exception e) {
            return RecurringTarget.NONE;
        }
    }

    public static boolean parseDays(Reminder reminder, String text) {
        Pattern p = Pattern.compile("(sun|mon|tues|wednes|thurs|fri|satur)days", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);

        boolean found = m.find();
        if (found) {
            reminder.recurring = true;
            String DOW = m.group().trim();

            int target = 0;
            if (DOW.startsWith("mon")) {
                target = Calendar.MONDAY;
                reminder.repeatTarget = RecurringTarget.MONDAYS;
            } else if (DOW.startsWith("tue")) {
                target = Calendar.TUESDAY;
                reminder.repeatTarget = RecurringTarget.TUESDAYS;
            } else if (DOW.startsWith("wed")) {
                target = Calendar.WEDNESDAY;
                reminder.repeatTarget = RecurringTarget.WEDNESDAYS;
            } else if (DOW.startsWith("thu")) {
                target = Calendar.THURSDAY;
                reminder.repeatTarget = RecurringTarget.THURSDAYS;
            } else if (DOW.startsWith("fri")) {
                target = Calendar.FRIDAY;
                reminder.repeatTarget = RecurringTarget.FRIDAYS;
            } else if (DOW.startsWith("sat")) {
                target = Calendar.SATURDAY;
                reminder.repeatTarget = RecurringTarget.SATURDAYS;
            } else if (DOW.startsWith("sun")) {
                target = Calendar.SUNDAY;
                reminder.repeatTarget = RecurringTarget.SUNDAYS;
            }
            reminder.repeatFrequency = 1;
            reminder.date.add(Calendar.DATE, 1);
            while(reminder.date.get(Calendar.DAY_OF_WEEK)!=target) {
                reminder.date.add(Calendar.DATE, 1);
            }
        }
        reminder.inflate();
        return true;
    }
}
