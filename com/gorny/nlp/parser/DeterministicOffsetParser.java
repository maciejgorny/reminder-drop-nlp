package com.gorny.nlp.parser;

import com.gorny.nlp.CalendarUtils;
import com.gorny.nlp.Reminder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Parser handles offset specified via pre-determined amount of time.
 * i.e. "the night before", or "the day after"
 */
public class DeterministicOffsetParser {

    public static String _N_ = "(^|\\s+)\\d+\\s+";

    public static boolean parse(Reminder reminder, String text)
    {
        System.out.println("DeterministicOffsetParser calc offset from \""+text+"\"");

        String shift = null;
        int coefficient = 0;

        /*before pattern*/

        Pattern p = Pattern.compile("(\\s+\\d+\\s+)*(day|week|month|night|morning|evening)(s)*\\s(before|ahead|early|earlier|in advance|ahead of time)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        if (m.find()) {
            shift = m.group().toUpperCase();
            coefficient = -1;

        } else {

        /*after pattern*/

            p = Pattern.compile("(\\s+\\d+\\s+)*(day|week|month|night|morning|evening)(s)*\\s(after|past|pass|passed|later)", Pattern.CASE_INSENSITIVE);
            m = p.matcher(text);
            if (m.find()) {
                shift = m.group().toUpperCase();
                coefficient = 1;
            }
        }

        //determine if there is a coefficient quantity
        p = Pattern.compile(_N_);
        m = p.matcher(text);
        if (m.find()) {
            String extr = m.group().trim();
            int multiplier = Integer.parseInt(extr);
            if (multiplier>0)
                coefficient *= multiplier;
        }

        if (coefficient != 0) {

            if (reminder.finalOffset == null)
                reminder.finalOffset = reminder.new FinalOffset();

            if (shift.startsWith("DAY")) {
                reminder.finalOffset.days = 1 * coefficient;
                reminder.finalOffset.hours = AmbiguousTime.MORNING;
                reminder.finalOffset.minutes = AmbiguousTime.NON_ZERO_MINUTES;
            } else if (shift.startsWith("WEEK")) {
                reminder.finalOffset.days = 7 * coefficient;
                reminder.finalOffset.hours = AmbiguousTime.MORNING;
                reminder.finalOffset.minutes = AmbiguousTime.NON_ZERO_MINUTES;
            } else if (shift.startsWith("MONTH")) {
                reminder.finalOffset.days = 30 * coefficient;
                reminder.finalOffset.hours = AmbiguousTime.MORNING;
                reminder.finalOffset.minutes = AmbiguousTime.NON_ZERO_MINUTES;
            }
            if (shift.startsWith("NIGHT")) {
                reminder.finalOffset.days = 1 * coefficient;
                reminder.finalOffset.hours = AmbiguousTime.NIGHT;
                reminder.finalOffset.minutes = AmbiguousTime.NON_ZERO_MINUTES;

            } else if (shift.startsWith("MORNING")) {
                reminder.finalOffset.days = 1 * coefficient;
                reminder.finalOffset.hours = AmbiguousTime.MORNING;
                reminder.finalOffset.minutes = AmbiguousTime.NON_ZERO_MINUTES;

            } else if (shift.startsWith("EVENING")) {
                reminder.finalOffset.days = 1 * coefficient;
                reminder.finalOffset.hours = AmbiguousTime.EVENING;
                reminder.finalOffset.minutes = AmbiguousTime.NON_ZERO_MINUTES;

            }
        }

        return true;
    }
}
