package com.gorny.nlp.parser;

import com.gorny.nlp.CalendarUtils;
import com.gorny.nlp.Reminder;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeShiftDirect {

    public static boolean parse(Reminder reminder, String text)
    {
        /*
            [within|next] <quantity> <shift amount>, i.e. "next year" --> "next 1 year"
         */
        Pattern p = Pattern.compile("second|minute|hour|day|week|month|year", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);

        boolean found = m.find();
        if (found)
        {
            String timeUnit = m.group().toUpperCase();
            CalendarUtils.TIME_UNIT time = CalendarUtils.TIME_UNIT.valueOf(timeUnit);

            p = Pattern.compile("\\d+");
            m = p.matcher(text);
            int amount = 1;
            if (m.find())
            {
                String N = m.group();
                amount = Integer.parseInt(N);
            }

            reminder.date.add(time.unit(), amount);

            if (time.unit() == Calendar.YEAR) {
                //i.e. "let's do this again next year"
                reminder.date.add(Calendar.DATE, -1);
            }
            else if (time.unit() == Calendar.MONTH) {
                //rollback to the first of the next month
                while (reminder.date.get(Calendar.DAY_OF_MONTH) != 1)
                    reminder.date.add(Calendar.DAY_OF_WEEK, -1);
            }
            else if (time.unit() == Calendar.WEEK_OF_YEAR) {
                //rollback to the first of the Monday of that week
                while (reminder.date.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
                    reminder.date.add(Calendar.DAY_OF_WEEK, -1);
            }
            CalendarUtils.resetTimeIfFutureAndAmbiguous(reminder);
        }
        reminder.inflate();
        return found;
    }
}
