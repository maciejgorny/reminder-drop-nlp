package com.gorny.nlp;

import com.gorny.nlp.parser.AmbiguousTime;

import java.util.Calendar;

public class ReminderRules {

    private String    _utterance = null;
    private String    _text      = null;
    private Reminder  _reminder  = null;

    public ReminderRules(String input) {
        this(input, null);
    }

    public ReminderRules(String input, String tz) {
        _reminder  = new Reminder(tz);
        _text = _utterance = Normalization.normalize(input.toLowerCase());
	_reminder.utterance = input;
    }
    
    
    public ReminderRules apply(Rule rule) {
        if (rule.foundIn(_text, _utterance)) {
            //process the rule
            if (rule.process(_reminder)) {
                System.out.println(rule.toString() + ", text=\""+_text+"\"");
                //clean up continuous white space gaps
                _text = _text.replaceFirst(rule.expression(), " ").replaceAll("\\s+", " ");
                System.out.println("       --> text=\""+_text+"\"");
                System.out.println("       --> date=\""+_reminder.year+"-"+_reminder.month+"-"+_reminder.day+"\"");
                System.out.println("       --> time=\""+_reminder.hour+":"+_reminder.minute+"\"");
                System.out.println("       --> haveDay=\""+_reminder.haveDay+", haveTime="+_reminder.haveTime+"\"");
            }
        }
        return this;
    }

    public Reminder getReminder() {
        if (_reminder.text == null ||
                (_reminder.text != null && _reminder.text.trim().length()==0))
            _reminder.text = _text.trim();

        Calendar now = Calendar.getInstance();

        /* update status and hints
         */
        if (_text.equals(_utterance)) {
            _reminder.status = Reminder.ReminderStatus.NO_REMINDER_DETECTED;
        }
        else
            _reminder.refreshStatus();

        /* if ambiguous time was used to fill a specific time, adjust the hint to indicate that
         */
        if (!_reminder.haveTime)
            _reminder.hint = Reminder.Hint.ASK_FOR_TIME;

        /* if no call to action is found adjust the hint to indicate that
         */
        if (_reminder.callToAction == null) {
            if (_reminder.hint.isEmpty())
                _reminder.hint = Reminder.Hint.USE_CALL_TO_ACTION;
            else
                _reminder.hint.append(Reminder.Hint.USE_CALL_TO_ACTION);
        }

        /* this will advance ambiguous LAPSED reminder to the next day,
         */
        if (!_reminder.haveDay && _reminder.haveTime && _reminder.status == Reminder.ReminderStatus.LAPSED) {
            //_reminder.date.add(Calendar.DAY_OF_MONTH, 1);
            CalendarUtils.ffwdToNextAmPm(_reminder);
            _reminder.refreshStatus();
            _reminder.inflate();
        }

        /* if it is not a reminder, but it has a date and a time, chances are it is an event
         */
        if (_reminder.status == Reminder.ReminderStatus.NO_REMINDER_DETECTED && _reminder.haveDay && _reminder.haveTime) {
            _reminder.status = Reminder.ReminderStatus.EVENT_DETECTED;
        }

        /* if no call to action is provided chances are either it is not a reminder, or it is time update for a previous reminder - hint it as such
         */
        if (_reminder.callToAction == null) {
            if (!_reminder.haveTime && !_reminder.haveDay)
                _reminder.checkIsTimeUpdate();
            else if (_reminder.haveTime && !_reminder.haveDay)
                    if (!_reminder.hint.getAll().contains(Reminder.Hint.TIME_UPDATE.name()))
                        _reminder.hint.append(Reminder.Hint.TIME_UPDATE);
        }

        /* apply early date/time offsets
         */
        if (_reminder.finalOffset != null) {
            System.out.println("Applying final offset, haveDay="+_reminder.haveDay+", haveTime="+_reminder.haveTime);
            if (!_reminder.haveTime) {
                if (_reminder.finalOffset.minutes != 0)
                    _reminder.date.set(Calendar.MINUTE, _reminder.finalOffset.minutes);

                if (_reminder.finalOffset.hours != 0)
                    _reminder.date.set(Calendar.HOUR_OF_DAY, _reminder.finalOffset.hours);
            }
            if (_reminder.finalOffset.days != 0)
                _reminder.date.add(Calendar.DAY_OF_MONTH, _reminder.finalOffset.days);

            if (_reminder.finalOffset.weeks != 0)
                _reminder.date.add(Calendar.DAY_OF_MONTH, _reminder.finalOffset.weeks * 7);

            if (_reminder.finalOffset.months != 0)
                _reminder.date.add(Calendar.MONTH, _reminder.finalOffset.months);

            if (_reminder.finalOffset.years != 0)
                _reminder.date.add(Calendar.YEAR, _reminder.finalOffset.years);

            if (_reminder.finalOffset.minutesDiff != 0)
                _reminder.date.add(Calendar.MINUTE, _reminder.finalOffset.minutesDiff);

            if (now.after(_reminder.date))
                _reminder.status = Reminder.ReminderStatus.LAPSED;

            _reminder.inflate();
        }

        if (_reminder.recurring && _reminder.status == Reminder.ReminderStatus.LAPSED) {
            System.out.println("GETREMINDER adjusting recurring LAPSED event to the next occurence");
            CalendarUtils.setNextEventForRecurringReminder(_reminder);
            if (!_reminder.haveTime && CalendarUtils.isTomorrowOrLater(_reminder.date)) {
                _reminder.date.set(Calendar.HOUR_OF_DAY, 6);
                _reminder.date.set(Calendar.MINUTE, 0);
            }
            _reminder.inflate();
            _reminder.refreshStatus();
        }

        if (!_reminder.haveTime && _reminder.finalOffset == null && _reminder.status != Reminder.ReminderStatus.NO_REMINDER_DETECTED)
        {
            if (!CalendarUtils.isTomorrowOrLater(_reminder.date))
            {
                Calendar tomorrow = Calendar.getInstance();
                tomorrow.add(Calendar.DATE, 1);
                if (now.after(_reminder.date))
                {

                    if (_reminder.date.get(Calendar.DATE) == tomorrow.get(Calendar.DATE) || _reminder.date.after(tomorrow)) {
                        _reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.MORNING);
                        _reminder.date.set(Calendar.MINUTE, 0);
                    }

                    if (_reminder.date.get(Calendar.DATE) == now.get(Calendar.DATE)) {
                        //pick the next avail time
                        Calendar next = Calendar.getInstance();
                        next.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NOON);
                        next.set(Calendar.MINUTE, 0);
                        if (now.before(next)) {
                            _reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NOON);
                            _reminder.date.set(Calendar.MINUTE, 0);
                            //_reminder.status = Reminder.ReminderStatus.FUTURE;
                        } else {
                            next.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTER_LUNCH);
                            if (now.before(next)) {
                                _reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.AFTER_LUNCH);
                                _reminder.date.set(Calendar.MINUTE, 0);
                                //_reminder.status = Reminder.ReminderStatus.FUTURE;
                            } else {
                                next.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
                                if (now.before(next)) {
                                    _reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.EVENING);
                                    _reminder.date.set(Calendar.MINUTE, 0);
                                    //_reminder.status = Reminder.ReminderStatus.FUTURE;
                                } else {
                                    next.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
                                    if (now.before(next)) {
                                        _reminder.date.set(Calendar.HOUR_OF_DAY, AmbiguousTime.NIGHT);
                                        _reminder.date.set(Calendar.MINUTE, 0);
                                        //_reminder.status = Reminder.ReminderStatus.FUTURE;
                                    } else {
                                        //just give up, ran out of time for "today"
                                        //noop
                                    }
                                }
                            }
                        }
                    }
                    _reminder.inflate();
                    _reminder.refreshStatus();
                }
            }
        }

        System.out.println("GETREMINDER, text=\""+_text+"\"");
        System.out.println("       --> date=\""+_reminder.year+"-"+_reminder.month+"-"+_reminder.day+"\"");
        System.out.println("       --> time=\""+_reminder.hour+":"+_reminder.minute+"\"");
        System.out.println("       --> utc="+_reminder.utcTime);

        return _reminder;
    }
}
