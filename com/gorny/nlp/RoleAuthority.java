package com.gorny.nlp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RoleAuthority {

    /** Function takes a string containing a number (NLP detected a number) and
     * determines if in fact it has a time role.
     *
     * i.e. "at 12" --> true
     *      "12 copies" --> false
     *      "the 12th time" --> false
     *      "at 12" --> true
     *      "tomorrow" --> true
     *      "tomorrow's" --> false
     */
    public static boolean isTimeRole(String phrase, String context) {

        //i.e. tomorrow's meeting, 's is indicative of an adjective, not time
        if (phrase.endsWith("'s"))
            return false;

        if (phrase.equals("morning")||phrase.equals("evening")||phrase.equals("afternoon")) {

            //in the morning|afternoon|evening
            Pattern p = Pattern.compile("\\s(in|in the)\\s"+phrase);
            Matcher m = p.matcher(context);
            if (m.find())
                return true;

            //on friday morning|afternoon|evening
            p = Pattern.compile("(on)*\\s(tomorrow|(mon|tues|wednes|fri|sat|sun)day)\\s"+phrase);
            m = p.matcher(context);
            if (m.find())
                return true;

            //"at <time> morning" (in this case morning|afternoon|evening is a AM/PM differentiator
            p = Pattern.compile("(at|@)\\s\\d+:\\d+\\s"+phrase);
            m = p.matcher(context);
            if (m.find())
                return true;

            return false;
        }

        return true;
    }

    public static boolean isTimeOffsetRole(String phrase, String context) {

        return true;
    }

    public static boolean hasTimeOfDayIndicator(String text) {
        Pattern p = Pattern.compile("\\d?((A|a)(M|m)|(P|p)(M|m))");
        Matcher m = p.matcher(text);
        return m.find();
    }

    public static boolean isRecurringRole(String phrase, String context) {

        return true;
    }


}
