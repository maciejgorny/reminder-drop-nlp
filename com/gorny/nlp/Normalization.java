package com.gorny.nlp;

import com.gorny.nlp.parser.AmbiguousTime;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Normalization
{
    public static String normalize(String phrase) {
        String normalized = phrase
                .replaceAll("(P|p)\\.(M|m)\\.", "pm")
                .replaceAll("(A|a)\\.(M|m)\\.", "am")
                //.replaceAll("[.,;:?\\/`@!#$%^&*()-+<>]+", " ") //strips punctuation (note that we need a.m.)
                .replaceAll("[!:\\.;,]", " ") //strip sentence dividing punctuation

                .replaceAll("in\\s+((a|one)?)\\s*minute", " in 1 minute ")
                .replaceAll("in\\s+(a|an|the|one)?\\s*hour", " in 1 hour ")
                .replaceAll("in\\s+((a|one)?)\\s+day", " in 24 hours ")
                .replaceAll("in(\\sa)*\\s+(half)?\\s?(a|an)?\\s+hour", " in 30 minutes ")
                .replaceAll("\\s+(half)\\s+(a|an)?\\s+hour", " 30 minutes ")
                .replaceAll("a half hour(s?)", "30 minutes")
                .replaceAll("\\s+(a|at|at a)\\s+quarter to midnight(\\s+|$)" , " at 23:45 ")
                .replaceAll("\\s([a]? quarter after midnight)(\\s+|$)", " 0:15 am ")
                .replaceAll("\\s(just before midnight)(\\s+|$)", " at 23:55 ")
                .replaceAll("\\s(before midnight)(\\s+|$)" , " before " + AmbiguousTime.BEFORE_MIDNIGHT_HR + ":" + AmbiguousTime.BEFORE_MIDNIGHT_MN+" pm ")
                .replaceAll("\\s(by midnight)(\\s+|$)" , " at " + AmbiguousTime.BEFORE_MIDNIGHT_HR + ":" + AmbiguousTime.BEFORE_MIDNIGHT_MN+" pm ")
                .replaceAll("\\s(after midnight)(\\s+|$)", " at " + AmbiguousTime.AFTER_MIDNIGHT_HR + ":" + AmbiguousTime.AFTER_MIDNIGHT_MN + " am ")
                .replaceAll("\\s(@)?(midnight)(\\s+|$)" , " 24:00 am ")
                .replaceAll("\\s(@)?(noon)(\\s+|$)", " 12:00 pm ")
                .replaceAll("(by|towards|at)(\\s+the)*\\s+end\\s+of(\\s+the)*\\s+day", " at " + AmbiguousTime.END_OF_DAY_HOUR + ":" + AmbiguousTime.END_OF_DAY_MINS + " pm ")

                .replaceAll("\\s(one)(\\s+|$)", " 1 ")
                .replaceAll("\\s(two)(\\s+|$)",       " 2 ")
                .replaceAll("\\s(three)(\\s+|$)"    , " 3 ")
                .replaceAll("\\s(four)(\\s+|$)"     , " 4 ")
                .replaceAll("\\s(five)(\\s+|$)"     , " 5 ")
                .replaceAll("\\s(six)(\\s+|$)"      , " 6 ")
                .replaceAll("\\s(seven)(\\s+|$)"    , " 7 ")
                .replaceAll("\\s(eight)(\\s+|$)", " 8 ")
                .replaceAll("\\s(nine)(\\s+|$)"     , " 9 ")
                .replaceAll("\\s(ten)(\\s+|$)"      , " 10 ")
                .replaceAll("\\s(eleven)(\\s+|$)"   , " 11 ")
                .replaceAll("\\s(twelve)(\\s+|$)"   , " 12 ")
                .replaceAll("\\s(thirteen)(\\s+|$)" , " 13 ")
                .replaceAll("\\s(fourteen)(\\s+|$)" , " 14 ")
                .replaceAll("\\s(fifteen)(\\s+|$)"  , " 15 ")
                .replaceAll("\\s(sixteen)(\\s+|$)"  , " 16 ")
                .replaceAll("\\s(seventeen)(\\s+|$)", " 17 ")
                .replaceAll("\\s(eighteen)(\\s+|$)", " 18 ")
                .replaceAll("\\s(nineteen)(\\s+|$)" , " 19 ")
                .replaceAll("\\s(twenty)(\\s+|$)"   , " 20 ")
                .replaceAll("\\s(thirty)(\\s+|$)"   , " 30 ")
                .replaceAll("\\s(fourty)(\\s+|$)"   , " 40 ")
                .replaceAll("\\s(forty)(\\s+|$)"    , " 40 ") //misspelled words, or words with non eng-us spelling, could be another pass for normalization
                .replaceAll("\\s(fifty)(\\s+|$)", " 50 ")

                .replaceAll("\\s(M|m)on\\.?\\s+", " monday ")
                .replaceAll("\\s(T|t)ue\\.?\\s+", " tuesday ")
                .replaceAll("\\s(W|w)ed\\.?\\s+", " wednesday ")
                .replaceAll("\\s(T|t)hu\\.?\\s+", " thursday ")
                .replaceAll("\\s(F|f)ri\\.?\\s+", " friday ")
                .replaceAll("\\s(S|s)at\\.?\\s+", " saturday ")
                .replaceAll("\\s(S|s)un\\.?\\s+", " sunday ")

                .replaceAll("\\s((J|j)an)\\.?\\s+", " January ")
                .replaceAll("\\s((F|f)eb)\\.?\\s+", " February ")
                .replaceAll("\\s((M|m)ar)\\.?\\s+", " March ")
                .replaceAll("\\s((A|a)pr)\\.?\\s+", " April ")
                .replaceAll("\\s((M|m)ay)\\.?\\s+", " May ")
                .replaceAll("\\s((J|j)un)\\.?\\s+", " June ")
                .replaceAll("\\s((J|j)ul)\\.?\\s+", " July ")
                .replaceAll("\\s((A|a)ug)\\.?\\s+", " August ")
                .replaceAll("\\s((S|s)ep)\\.?\\s+", " September ")
                .replaceAll("\\s((O|o)ct)\\.?\\s+", " October ")
                .replaceAll("\\s((N|n)ov)\\.?\\s+", " November ")
                .replaceAll("\\s((D|d)ec)\\.?\\s+", " December ")
                ;
        normalized = normalize_laterWordDrop(normalized);
        normalized = normalize_afterLunch(normalized);
        normalized = normalize_in_the_timeOfDay(normalized);
        normalized = normalize_everyTimeOfDay_to_everyDay(normalized);
        normalized = normalize_at_sign(normalized);
        normalized = normalize_spoken_numerals(normalized);
        normalized = normalize_ToLiteral(" minutes ", normalized);
        normalized = normalize_ToLiteral(" hours ", normalized);
        normalized = N45_normalize(normalized);
        normalized = NtoM_normalize(normalized);
        normalized = N15_normalize(normalized);
        normalized = NM_normalize(normalized);
        normalized = N30_normalize(normalized);
        normalized = normalize_N_FromNow(normalized);
        normalized = normalize_ambiguous_time_shift(normalized);
        normalized = normalize_At_d(normalized);
        normalized = normalize_everyOther(normalized);

        System.out.println("NORMALIZED, text=\""+normalized+"\"");
        return normalized;
    }

    /*
     * "after lunch" can act as a specific time, or a p.m. indicator
     * (i.e. "at 2:30 after lunch")
     */
    public static String normalize_afterLunch(String string) {
        Pattern p = Pattern.compile("\\s+(\\d{1,2}(:|\\s)\\d{1,2}\\s)(after\\s+lunch(\\s+|$))");
        Matcher m = p.matcher(string);
        if (m.find()) {
            string = string.replace("after lunch", "p.m.");
        } else {
            p = Pattern.compile("\\s+(\\d{1,2}:\\d{1,2}\\s)(after\\s+lunch(\\s+|$))");
            m = p.matcher(string);
            if (m.find()) {
                string = string.replaceAll("\\s+after\\s+lunch(\\s+|$)", " at " + AmbiguousTime.AFTER_LUNCH + ":00 pm ");
            } else {
                string = string
                        .replaceAll("\\s+after\\s+lunch(\\s+|$)", " at " + AmbiguousTime.AFTER_LUNCH+":00 pm ")
                        .replaceAll("\\s(@)?(lunch time)(\\s+|$)", " 12:00 pm ")
                        .replaceAll("\\s(@)?(lunch)(\\s+|$)", " 12:00 pm ");
            }
        }
        return string;
    }

    /**
     * Drop the word "later" if a time phrase (ambiguous or not) follows.
     * For instance: "remind me to do this and that <b>later tonight</b>", the user really means "remind me to do this and that <b>tonight</b>".
     * Same applies to all other time entities.
     * test cases:
     *              "... later today"
     *              "... later on tonight"
     *              "... later on in the afternoon"
     */
    public static String normalize_laterWordDrop(String string) {
        Pattern p = Pattern.compile("later\\s+(on\\s+)*(this\\s+)*(in\\s+the\\s+)*(today|tonight|morning|afternoon|evening)");
        Matcher m = p.matcher(string);
        if (m.find()) {
            String group = m.group();
            String target = group.startsWith("later on") ? "later on" : "later" ; //<-- TODO: fix this to build and replace using regex to cover other cases
            string = string.replace(target, "");
        }
        return string;
    }

    /*
     * "at 9 in the evening" --> "at 9 pm"
     */
    private static String normalize_in_the_timeOfDay(String string) {
        Pattern p = Pattern.compile("((in\\s+the|at)\\s+)*(\\d\\s+in\\s+the)*((morning|evening|afternoon)s?)\\s+(around|at)\\s+(\\d+(\\s|:*)\\d*)\\s*(am|pm|a.m.|p.m.)*(\\s+|$)", Pattern.CASE_INSENSITIVE);

        /*
          FAIL:
          The pattern above does not match:
          "i will call you thursday morning around 8 am to talk about the new initiative"

          try:
          (in\sthe|at)*\s(morning|evening|afternoon)\s+(around|at)\s\d{1,2}\s*(am|pm)*(\s+|$)
        */

        Matcher m = p.matcher(string);
        if (m.find()) {
            String group = m.group();
            String ampm = "";
            String tod = null; //time of day

            //check if group already contain am/pm designation
            p = Pattern.compile("\\s+(\\d(\\s+)*)*(am|pm)\\s+", Pattern.CASE_INSENSITIVE);
            m = p.matcher(group);
            boolean hasAmPm = m.find();

            if (group.contains("morning")) {
                if (!hasAmPm)
                    ampm = " am";
                tod = "in the morning";
            } else if (group.contains("afternoon")) {
                if (!hasAmPm)
                    ampm = " pm";
                tod = "in the afternoon";
            } else if (group.contains("evening")) {
                if (!hasAmPm)
                    ampm = " pm";
                tod = "in the evening";
            }
            string = string.replace(tod, ampm);
        }
        return string;
    }

    /**
     * "mornings at 9"
     *
     * "every morning at 9" --> "every day at 9 am", etc.
     *
     * "every (monring|afternoon|evening)" --> "every day at (MORNING|AFTERNOON|EVENING)(am|pm)"
     *
     * "every morning at 9 am" --> "every day at 9 am" (replace "every morning" -> "every day" since am|pm is provided)
     *
     */
    private static String normalize_everyTimeOfDay_to_everyDay(String string) {
        String days = "(mon|tues|wednes|thurs|fri|satur|sun|week|weekend)day";

        //allows all words in-between except "other"
        //
        Pattern p = Pattern.compile("((((every|each)\\s+((?!other)(\\w+)\\s+)*)(morning|afternoon|evening)|(mornings|afternoons|evenings)))(\\s+at\\s+\\d(\\s*(am|pm))*)*");
        Matcher m = p.matcher(string);
        if (m.find()) {
            String group = m.group();
            String hr = null;

            p = Pattern.compile("\\d{1,2}");
            m = p.matcher(group);
            if (m.find()) {
                hr = m.group().trim();
            }

            String ampm = null;
            String tod = null; //time of day

            p = Pattern.compile("(in\\s+the\\s+)*(morning|afternoon|evening)", Pattern.CASE_INSENSITIVE);
            m = p.matcher(group);
            if (m.find()) {
                tod = m.group().trim();
                if (group.contains("morning")) {
                    ampm = (hr == null ? AmbiguousTime.MORNING : "") + " am";
                    //tod = "morning";
                } else if (group.contains("afternoon")) {
                    ampm = (hr == null ? AmbiguousTime.AFTERNOON : "") + " pm";
                    //tod = "afternoon";
                } else if (group.contains("evening")) {
                    ampm = (hr == null ? AmbiguousTime.EVENING : "") + " pm";
                    //tod = "evening";
                }
            }

            p = Pattern.compile("(mon|tues|wednes|thurs|fri|satur|sun|week|weekend)day", Pattern.CASE_INSENSITIVE);
            m = p.matcher(group);
            if (m.find()) {
                group = m.group();
                string = string.replace(tod, (hr==null ? " at " : "") + ampm);
                string = internal_normalize_dayofweek(group, string);
            } else {
                if (hr != null && ampm !=null)
                    string = string.replace(group, "every day at " + hr + ampm);
            }

        } else {
            //special case for handling phrases with the word "other" (i.e. "every other day")
            //
            p = Pattern.compile("(every\\s+other\\s+(morning|afternoon|evening))|(mornings|afternoons|evenings)");
            m = p.matcher(string);
            if (m.find()) {
                p = Pattern.compile("morning|afternoon|evening");
                m = p.matcher(string);
                if (m.find()) {
                    String group = m.group();
                    string = string.replace(group, "day");
                }
            }

            //every Saturday in the evening
            //every Saturday at 9 in the evening
            //every Saturday at 9 pm
            //                  |____________--> Saturdays at 9 pm
            p = Pattern.compile("(every|each)\\s+((?!each)(\\w+\\s+))*(mon|tues|wednes|Thurs|fri|satur|sun|week|weekend)day", Pattern.CASE_INSENSITIVE);
            m = p.matcher(string);
            if (m.find()) {
                String group = m.group();

                //strip \w+ between "every" and "target-day"
                String[] atom = group.split("\\s+");

                if (atom.length > 2) {

                    String middle = atom[1];
                    //TODO: check if \w+ is "other" and set repeatFrequency to 2 if true
                    if ("other".equals(middle)) {
                        string = string.replace("other", "2nd");
                    }
                    //check if \w+ is a numeric describing frequency (second, 2nd, third,  and update repeatFrequency accordingly

                } else if (atom.length == 2) {

                    group = atom[1];

                } else {

                    //more than one word between "every" and "target-day"??

                }

                //TODO: string \w+ out of string instance so internal normalization below passes

                string = internal_normalize_dayofweek(group, string);
            }
        }

        return string;
    }

    private static String internal_normalize_dayofweek(String group, String string) {
        if ("monday".equals(group))
            return string.replaceAll("(^|\\s)(every|each)\\s+((M|m)on)day(\\s+|$)", " mondays ");
        if ("tuesday".equals(group))
            return string.replaceAll("(^|\\s)(every|each)\\s+((T|t)ue)day(\\s+|$)", " tuesdays ");
        if ("wednesday".equals(group))
            return string.replaceAll("(^|\\s)(every|each)\\s+((W|w)ednes)day(\\s+|$)", " wednesdays ");
        if ("thursday".equals(group))
            return string.replaceAll("(^|\\s)(every|each)\\s+((T|t)hurs)day(\\s+|$)", " thursdays ");
        if ("friday".equals(group))
            return string.replaceAll("(^|\\s)(every|each)\\s+((F|f)ri)day(\\s+|$)", " fridays ");
        if ("saturday".equals(group))
            return string.replaceAll("(^|\\s)(every|each)\\s+((S|s)atur)day(\\s+|$)", " saturdays ");
        if ("sunday".equals(group))
            return string.replaceAll("(^|\\s)(every|each)\\s+((S|s)un)day(\\s+|$)", " sundays ");
        return string;
    }

    private static String normalize_everyOther(String string) {
        Pattern p = Pattern.compile("(every\\s+other\\s+(minute|hour|day|week|weekend|weekday|month|year))", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        if (m.find()) {
            String group = m.group().trim();
            String normalized_group = group.replace("other", "2").concat("s");
            string = string.replace(group, normalized_group);
        }
        return string;
    }

    /**
     * Changes @9am to at 9am
     */
    private static String normalize_at_sign(String string) {
        Pattern p = Pattern.compile("\\s+@\\s*(\\d{1,2}(:\\d{1,2})*)\\s*((A|P|a|p)m)*", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        if (m.find()) {
            string = string.replace("@", " at ");
        }
        return string;
    }

    /**
     * Normalizes "minutes" entity
     */
    private static String normalize_ToLiteral(String literal, String string) {
        String regex = null;
        String replaceTarget = null;
        if (" minutes ".equals(literal))
        {
            // We need a valid time entity, there are other meaning for "min",
            // TODO: maybe this part should be handled by the RoleAuthority
            //regex = "(in\\s+)*\\d+\\s+(min(.)?|mins)(\\s+(before|after|later)*)";
            //replaceTarget = "\\s+(min(\\.)?|mins)\\s+";

            regex = "in(\\s+about)*\\s+\\d+\\s+min(s?)(\\.)?|\\d+\\s+min(s?)(\\.)?(\\s)*(before|earlier|later)";
            replaceTarget = "\\s+min(s?)(\\.)?(\\s+|$)";
        }
        else if (" hours ".equals(literal))
        {
            regex = "\\d+\\s+(hr(s?)(\\.)?)";
            replaceTarget = "\\s+(hr(s?)(\\.)?)\\s+";
        }
        if (regex != null && replaceTarget != null) {
            Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(string);
            if (m.find()) {
                string = string.replaceAll(replaceTarget, literal);
            }
        }
        return string;
    }

    /**
     * Pattern: "M to N" ==> "(N-1):(60-M)"
     */
    private static String NtoM_normalize(String string) {
        Pattern p = Pattern.compile("at\\s+\\d+\\s+to\\s+\\d+" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        boolean found = m.find();
        if (found) {
            String s = m.group();
            String[] MN = s.split(" ");
            int M = Integer.parseInt(MN[1]);
            int N = Integer.parseInt(MN[3]);
            String reduced = String.format("at %d:%d", (N-1), (60-M));
            string = string.replace(s, reduced);
        }
        return string;
    }

    /**
     * Pattern: "half past N" ==> "N:30"
     */
    private static String N30_normalize(String string)
    {
        Pattern p = Pattern.compile("half\\s+past\\s+\\d+", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        boolean found = m.find();
        if (found) {
            String s = m.group();
            String N = s.substring(s.lastIndexOf(" ")+1);
            String adapted = String.format("%s:30", N);
            string = string.replace(s, adapted);
        }
        return string;
    }

    /**
     * Pattern: "M after N" ==> "N:M"
     */
    private static String NM_normalize(String string)
    {
        Pattern p = Pattern.compile("\\d+\\s+after\\s+\\d+\\s*", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        boolean found = m.find();
        if (found) {
            String s = m.group();
            String[] MN = s.split(" ");
            String M = MN[0];
            String N = MN[2];
            String adapted = String.format("%s:%s", N, M);
            string = string.replace(s, adapted);
        }
        return string;
    }

    /**
     * Pattern: "quarter after N" == "N:15"
     */
    private static String N15_normalize(String string)
    {
        Pattern p = Pattern.compile("(at)*\\s+quarter\\s+after\\s+\\d+", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        boolean found = m.find();
        if (found) {
            String s = m.group();
            String N = s.substring(s.lastIndexOf(" ")+1);
            String adapted = String.format(" at %s:15", N);
            string = string.replace(s, adapted);
        }
        return string;
    }

    /**
     * Pattern: "quarter to N" ==> "(N-1):45"
     */
    private static String N45_normalize(String string) {
        Pattern p = Pattern.compile("(at)*(\\sa)*\\s+quarter\\s+to\\s+\\d+" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        boolean found = m.find();
        if (found) {
            String s = m.group();
            String number = s.substring(s.lastIndexOf(" ")+1);
            int n = Integer.parseInt(number);
            String reduced = String.format(" at %d:45", n-1);
            string = string.replace(s, reduced);
        }
        return string;
    }

    /**
     * Pattern: "in a* (few|couple of|several) M" ==> time shift by M
     */
    private static String normalize_ambiguous_time_shift(String string) {
        Pattern p = Pattern.compile("(in|in a)\\s+(few|couple of|several)\\s+(minutes|hours|days)\\s*" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        boolean found = m.find();
        if (found) {
            String s = m.group();
            string = string
                    .replaceFirst("a few"       , String.valueOf(AmbiguousTime.FEW))
                    .replaceFirst("few"         , String.valueOf(AmbiguousTime.FEW))
                    .replaceFirst("a couple of" , String.valueOf(AmbiguousTime.COUPLE_OF))
                    .replaceFirst("couple of"   , String.valueOf(AmbiguousTime.COUPLE_OF))
                    .replaceFirst("several"     , String.valueOf(AmbiguousTime.SEVERAL));
        }
        return string;
    }

    /**
     * [(half an hour|N minutes) from now]" ==> [in N minutes]
     */
    private static String normalize_N_FromNow(String string) {
        Pattern p = Pattern.compile("(half an hour|half hour|\\d+\\s+(minute(s?)|hour(s?)))?\\s+from now" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        boolean found = m.find();
        if (found) {
            String s = m.group();

            String normalized;
            if (s.startsWith("half an hour")||s.startsWith("half hour"))
                normalized = "in 30 minutes";
            else
                normalized = "in "+s;

            string = string.replace(s, normalized.replaceFirst("from now",""));
        }
        return string;
    }

    /**
     * "at 405" ==> "at 4:05"
     */
    private static String normalize_At_d(String string) {

        Pattern p1 = Pattern.compile("at\\s+\\d+:\\d+\\s*");
        Matcher m1 = p1.matcher(string);
        if (m1.find())
            return string;

        Pattern p = Pattern.compile("at\\s+\\d+\\s*" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        boolean found = m.find();
        if (found) {
            String s = m.group().trim();
            //at 425
            if (s.contains(":"))
                return string;
            //drop "at "
            String normalized = s.substring(3);
            if (normalized.length()<3)
                return string;
            //
            StringBuffer sb = new StringBuffer(normalized);
            sb.insert(normalized.length()-2,":");
            string = string.replace(normalized, sb.toString());
        }
        return string;
    }

    /**
     * "five thirty six" ==> "at 5:36"
     * "six twenty" ==> "6:20"
     */
    private static String normalize_spoken_numerals(String string) {

        /*
            "five thirty six" --> "5 30 6" --> "5:36"
         */
        Pattern p = Pattern.compile("\\d+\\s+\\d+(\\s+\\d+)?" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        boolean found = m.find();
        if (found) {
            String s = m.group();
            String[] digits = s.split("\\s+");

            Integer h = Integer.parseInt(digits[0]);
            Integer m1 = Integer.parseInt(digits[1]);
            Integer m2 = digits.length > 2 ? Integer.parseInt(digits[2]) : 0;

            String normalized;

            //do range check for hour and minutes fields
            if (h>=0 && h<=23 && m1>=0 && m1<=59 && m2>=0 && m2<=9) {
                StringBuilder sb = new StringBuilder(""+h);
                sb.append(":").append(""+(m1+m2));
                normalized = sb.toString();
                string = string.replaceFirst(s, normalized);
            }
        }
        return string;
    }


}
