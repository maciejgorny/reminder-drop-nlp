package com.gorny.nlp;

import com.gorny.nlp.parser.AmbiguousTime;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeParser
{
    public static final String REGEX_TIME_HOUR_AND_MINUTES = "([0-9]+)(:*[0-9]*)";

    public static final int AM_PM_NOT_SET = 2;

    public static final String AM  = "am";
    public static final String AM_ = "a.m.";
    public static final String PM  = "pm";
    public static final String PM_ = "p.m.";

    public static boolean parse(Reminder reminder, String text)
    {
        Pattern p = Pattern.compile(REGEX_TIME_HOUR_AND_MINUTES, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);

        boolean found = m.find();
        if (found) {
            String time = text.substring(m.start(), m.end());
            int hr, mn = 0;

            String[] parts = time.split(":");
            hr = Integer.parseInt(parts[0]);
            if (parts.length == 2)
                mn = Integer.parseInt(parts[1]);

            if (hr >= 0 && hr <= 24 && mn >= 0 && mn < 60) {
                reminder.date.set(Calendar.HOUR_OF_DAY, hr);
                reminder.date.set(Calendar.MINUTE, mn);

                if (text.startsWith("just before") || text.startsWith("right before"))
                    reminder.date.add(Calendar.MINUTE, AmbiguousTime.SHIFT_JUST_BEFORE);

                if (text.startsWith("just after") || text.startsWith("right after"))
                    reminder.date.add(Calendar.MINUTE, AmbiguousTime.SHIFT_JUST_AFTER);

                if (text.startsWith("before"))
                    reminder.date.add(Calendar.MINUTE, AmbiguousTime.SHIFT_BEFORE);

                if (text.startsWith("by"))
                    reminder.date.add(Calendar.MINUTE, AmbiguousTime.SHIFT_BY);

                if (text.startsWith("after") || text.startsWith("past") || text.startsWith("pass") || text.startsWith("passed"))
                    reminder.date.add(Calendar.MINUTE, AmbiguousTime.SHIFT_AFTER);

                if (reminder.ampm == AM_PM_NOT_SET && hr <= 12 && !text.contains(PM) && !text.contains(PM_) && !text.contains(AM) && !text.contains(AM_)) {
                    reminder.ampm = determineIntendedTimeOfDay(hr, mn, reminder);
                    //CalendarUtils.ffwdToNextAmPm(reminder);
                    reminder.hint.append(Reminder.Hint.AMBIGUOUS_TIME_OF_DAY);
                }

                text = text.trim(); //remove leading and trailing white space before proceeding here

                hr = reminder.date.get(Calendar.HOUR_OF_DAY);
                mn = reminder.date.get(Calendar.MINUTE);

                reminder.haveAmPm = RoleAuthority.hasTimeOfDayIndicator(text) || hr > 12 || reminder.haveAmPm;

                if (((text.contains(PM) || text.contains(PM_)) && hr < 12) || (reminder.ampm == Calendar.PM && hr < 12))//&& exceptWhen(reminder, hr, mn))
                    hr += 12;

                reminder.date.set(Calendar.HOUR_OF_DAY, hr);
                reminder.date.set(Calendar.MINUTE, mn);

                if (reminder.hint == Reminder.Hint.ASK_FOR_TIME) /* could have been set by ambiguous time tier*/
                    reminder.hint = Reminder.Hint.NONE;
            }
        }

        reminder.inflate();
        return found;
    }

    /**
     * Given only the time, this function determines the next available time of day (AM|PM) relative to the current time (now)!
     * so if an utterance specifies a time but not the time of day ("AM", "PM", "in the evening", etc.)
     * then the next valid time of day is determined.
     * @param hour the clock hour of the target time
     */

    public static int determineIntendedTimeOfDay(int hour, int min, Reminder reminder)
    {
        Calendar now = Calendar.getInstance();
        if (reminder.date.get(Calendar.DATE) == now.get(Calendar.DATE)) {

            int hourAM = hour < 12 ? hour : hour - 12;
            int hourPM = hour > 12 ? hour : hour + 12;

            if (now.get(Calendar.HOUR_OF_DAY) == hourPM || now.get(Calendar.HOUR_OF_DAY) == hourAM) {
                if (now.get(Calendar.MINUTE) < min)
                    return now.get(Calendar.AM_PM);
                else {
                    if (reminder.ampm == TimeParser.AM_PM_NOT_SET) { //TODO: debug this after 8 am before 9 am
                        return Calendar.PM;
                    }
                    else
                        return reminder.ampm == Calendar.AM ? Calendar.PM : Calendar.AM;
                }
            }
            else
            {
                int giveupctr = 90000; //25 hours later, we're not going to find it
                while (now.get(Calendar.HOUR_OF_DAY) != hourPM && now.get(Calendar.HOUR_OF_DAY) != hourAM) {
                    now.add(Calendar.MINUTE, 1);
                    System.out.println("    [TimeParser] ffwd time to: " + now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE));
                    giveupctr --;
                    if (giveupctr<=0)
                        break;
                }
                return now.get(Calendar.AM_PM);
            }
        }
        return reminder.ampm;
    }

}
