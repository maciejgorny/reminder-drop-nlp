package com.gorny.nlp;

import com.gorny.nlp.parser.AmbiguousTimeParser;
import com.gorny.nlp.parser.DayOfWeekParser;
import com.gorny.nlp.parser.DeterministicDayParser;
import com.gorny.nlp.parser.MessageBodyExtractor;
import com.gorny.nlp.parser.MonthOfYearParser;
import com.gorny.nlp.parser.RecurringParser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NamedEntities {
    public enum CALL_TO_ACTION implements Rule {
        REMIND_ME_TO    ("remind\\s+(\\w*)(\\s+(to|that|about))?"),
        TELL_ME_TO      ("tell\\s+(\\w*)(\\s+(to|that|about))?"),
        DONT_FORGET_TO  ("don't\\s+forget\\s+(to|that|about)?"),
        REMINDER_TO     ("reminder\\s+(to|that|about)"),
        REMEMBER_TO     ("remember\\s+(to|that|about)"),
        All             ("((remember|reminder|tell|remind)\\s+(me|us|\\S+)|(don't|do not)\\s+forget)\\s+(to|that|about)?"),
        ;

        private String _regex,_match;
        public boolean _found;
        private CALL_TO_ACTION(String regex) { _regex = regex; }

        public String expression() { return _regex; }
        public boolean foundIn(String input, String utterance) {
            Pattern p = Pattern.compile(_regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(input);
            _found = m.find();
            if (_found) {
                _match = m.group();
                _found = verifyRole(_match, utterance);
            }
            return _found;
        }
        public boolean process(Reminder reminder) {
            return _found ? reminder.parse(this, _match.trim()) : false;
        }
        public boolean verifyRole(String text, String context) {
            return true;
        }
    }

    public enum MESSAGE_BODY implements Rule {
        AFTER_ABOUT_THAT_TO   (MessageBodyExtractor.AFTER_ABOUT_THAT_TO),
        ;

        private String _regex, _match;
        public boolean _found;
        MESSAGE_BODY(String regex) { _regex = regex; }

        public String expression() { return _regex; }
        public boolean foundIn(String input, String utterance) {
            Pattern p = Pattern.compile(_regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(input);
            _found = m.find();
            if (_found) {
                _match = m.group();
                _found = verifyRole(_match, utterance);
            }
            return _found;
        }
        public boolean process(Reminder reminder) {
            return _found ? reminder.parse(this, _match.trim()) : false;
        }
        public boolean verifyRole(String text, String context) {
            return true;
        }
    }

    public enum TIME_SHIFT implements Rule {
        IN_N    ("\\sin\\b\\s[0-9a]*\\s(seconds|minutes|minute|hours|hour|days|day|weeks|week|months|month|years|year)(\\s+|$)"),
        NEXT_N  ("\\s(((of\\s+)*next)|within|within next)(\\s+(a|few|several|\\d))*\\s+(second|minute|hours|day|week|month|year)s*(\\s+|$)");

        private String _regex,_match;
        public boolean _found;
        private TIME_SHIFT(String regex) { this._regex = regex; }

        public String expression() { return _regex; }
        public boolean foundIn(String input, String utterance) {
            Pattern p = Pattern.compile(_regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(input);
            _found = m.find();
            if (_found){
                _match = m.group();
                _found = verifyRole(_match, utterance);
            }
            return _found;
        }

        public boolean process(Reminder reminder) {
            return _found ? reminder.parse(this, _match.trim()) : false;
        }

        public boolean verifyRole(String text, String context) {
            return true;
        }

    }//TIME_SHIFT

    public enum TIME_PLACEMENT implements Rule {
        DETERMINISTIC_DAY       (DeterministicDayParser.EXPRESSION),
        AMBIGUOUS_TIME          (AmbiguousTimeParser.EXPRESSION),
        DAY_OF_WEEK             (DayOfWeekParser.EXPRESSION),
        MONTH_OF_YEAR           (MonthOfYearParser.EXPRESSION),
        DAY_OF_MONTH            ("((O|o)n)*((\\s(((M|m)on|(T|t)ues|(W|w)ednes|(F|f)ri|(S|s)atur|(S|s)un)day))?\\s+)*((J|j)anuary|(f|F)ebruary|(m|M)arch|(a|A)pril|(m|M)ay|(j|J)une|(j|J)uly|(a|A)ugust|(s|S)eptember|(o|O)ctober|(n|N)ovember|(D|d)ecember)\\s+\\d*(st|rd|th)*(\\s+|,)*[\\d]+(st|rd|th)*(\\s+\\d+)*"),
        OF_DAY_OF_MONTH         ("on(\\s+the)*\\s+(\\d{1,2}(nd|rd|th)(\\s+of)*)(((\\s+day)*\\s+of\\s+)((J|j)anuary|(f|F)ebruary|(m|M)arch|(a|A)pril|(m|M)ay|(j|J)une|(j|J)uly|(a|A)ugust|(s|S)eptember|(o|O)ctober|(n|N)ovember|(D|d)ecember|((the\\s+)*month)))*"),
        TIME_OF_DAY             ("(at|before|after|right before|just before|just after|right after|by|around)+\\s+(\\d+:*\\d*)|\\s+(\\d+(\\s|:*)\\d*)\\s*(PM|pm|P.M.|p.m.|AM|am|A.M.|a.m.|o'clock|noon)+|\\s*((at|before|after|pass|past|passed|just before|just after|by|around)+\\s+(\\d+(\\s|:*)\\d*)\\s*(PM|pm|P.M.|p.m.|AM|am|A.M.|a.m.|o'clock|noon)+)");
        private String _regex,_match;
        public boolean _found;
        TIME_PLACEMENT(String regex) { this._regex = regex; }

        public String expression() { return _regex; }
        public boolean foundIn(String input, String utterance) {
            Pattern p = Pattern.compile(_regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(input);
            _found = m.find();
            if (_found) {
                _match = m.group();
                _found = verifyRole(_match, utterance);
            }
            return _found;
        }

        public boolean process(Reminder reminder) {
            return _found ? reminder.parse(this, _match.trim()) : false;
        }

        public boolean verifyRole(String text, String context) {
            return RoleAuthority.isTimeRole(text, context);
        }
    }

    public enum TIME_OFFSET implements Rule {
        /*remind me 30 minutes early*/
        BY_TIME ("\\d*\\s+(year|month|week|day|hour|minute|min|min.)(s?)\\s+(early|earlier|later|before|after|in advance|ahead|ahead of time|past|passed|pass)(\\s+|$)"),

        /*remind me the night|day|week|... before that...*/
        BY_DOW ("((\\s+\\d)|((a|the)?))*\\s+(year|month|week|day|night|morning|evening)(s)*\\s(before|ahead|after|early|earlier|in advance|ahead|ahead of time|past|passed|pass)\\s*?")
        ;
        private String _regex, _match;
        public boolean _found;
        private TIME_OFFSET(String regex) { this._regex = regex; }

        public String expression() { return _regex; }
        public boolean foundIn(String input, String utterance) {
            Pattern p = Pattern.compile(_regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(input);
            _found = m.find();
            if (_found) {
                _match = m.group();
                _found = verifyRole(_match, utterance);
            }
            return _found;
        }

        public boolean process(Reminder reminder) {
            return _found ? reminder.parse(this, _match.trim()) : false;
        }

        public boolean verifyRole(String text, String context) {
            return RoleAuthority.isTimeOffsetRole(text, context);
        }
    }


    public enum RECURRING implements Rule {
        /*every day, each day, every 3 days, once a month, every 20 minutes*/
        REPEAT_EVENT(RecurringParser.EVERY_N_REPEAT),

        /*Mondays, Tuesdays, ...*/
        DAYS(RecurringParser.DAYS)

        /*every|each Tuesday and Friday and Wednesday..., */
        ;
        private String _regex, _match;
        public boolean _found;
        private RECURRING(String regex) { this._regex = regex; }

        public String expression() { return _regex; }
        public boolean foundIn(String input, String utterance) {
            Pattern p = Pattern.compile(_regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(input);
            _found = m.find();
            if (_found) {
                _match = m.group();
                _found = verifyRole(_match, utterance);
            }
            return _found;
        }

        public boolean process(Reminder reminder) {
            return _found ? reminder.parse(this, _match.trim()) : false;
        }

        public boolean verifyRole(String text, String context) {
            return RoleAuthority.isRecurringRole(text, context);
        }
    }

}
