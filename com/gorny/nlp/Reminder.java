package com.gorny.nlp;

import com.gorny.nlp.parser.DeterministicDayParser;
import com.gorny.nlp.parser.AmbiguousTimeParser;
import com.gorny.nlp.parser.DayOfWeekParser;
import com.gorny.nlp.parser.DayOfYearParser;
import com.gorny.nlp.parser.DeterministicOffsetParser;
import com.gorny.nlp.parser.MessageBodyExtractor;
import com.gorny.nlp.parser.MonthOfYearParser;
import com.gorny.nlp.parser.QuantativeOffsetParser;
import com.gorny.nlp.parser.RecurringParser;
import com.gorny.nlp.parser.TimeShiftDirect;
import com.gorny.nlp.parser.TimeShiftParser;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reminder {

    public enum ReminderStatus {
        FUTURE,
        LAPSED,
        INVALID,
        NO_REMINDER_DETECTED,
        EVENT_DETECTED,
        INCOMPLETE
    }

    public enum Hint {
        NONE,
        ASK_FOR_TIME,
        AMBIGUOUS_TIME_OF_DAY,
        USE_CALL_TO_ACTION,
        TIME_UPDATE
        ;
        private String hint;
        Hint() { hint = name(); }
        public void clear()        { hint = name(); }
        public String getAll()     { return hint; }
        public void append(Hint h) {
            if (hint==NONE.name())
                hint=h.name();
            else if (!hint.contains(h.name()))
                hint += (hint.length()==0 ? "" : ",") + h;
        }
        public boolean isEmpty() {
            return hint == NONE.name();
        }
    }

    public String
            sessionId,
            text,
            utterance,
            callToAction,
            dateString,
            clientTimeZone;

    public Long
            utcTime,
            processTime;

    public Calendar
            date;

    public int
            year    = 0,
            month   = 0,
            day     = 0,
            hour    = 0,
            minute  = 0,
            ampm    = TimeParser.AM_PM_NOT_SET,

            repeatFrequency = 1;

    public boolean
            haveDay,
            haveTime,
            haveAmPm,
            recurring;

    public RecurringParser.RecurringTarget repeatTarget = RecurringParser.RecurringTarget.NONE;
    public FinalOffset finalOffset = null;

    public ReminderStatus
            status = ReminderStatus.INVALID;

    public Hint
            hint = Hint.NONE;

    /*
    Eastern Time            EST	America/New_York
    Central Time            CST	America/Chicago
    Mountain Time           MST	America/Denver
    Pacific Time            PST	America/Los_Angeles
    Alaska Time             AST	America/Anchorage
    Hawaii-Aleutian Time    HST	America/Adak
     */
    public Reminder() {
        this(null);
    }
    
    public Reminder(String tz) {
        if (tz != null) {
            clientTimeZone = tz;
            date = Calendar.getInstance(TimeZone.getTimeZone(tz));
        } else {
            date = Calendar.getInstance();
        }
        inflate();
    }

    public void inflate() {
        utcTime     = date.getTime().getTime() / 1000;//<-- we want epoch
        dateString  = date.toString();
        year        = date.get(Calendar.YEAR);
        month       = date.get(Calendar.MONTH) + 1; //<-- months are 0 based
        day         = date.get(Calendar.DAY_OF_MONTH);
        hour        = date.get(Calendar.HOUR_OF_DAY);
        minute      = date.get(Calendar.MINUTE);
    }

    public boolean parse(Rule rule, String string) {
        boolean altered = false;
        if (rule instanceof NamedEntities.CALL_TO_ACTION)
        {
            String actionCall = ((NamedEntities.CALL_TO_ACTION) rule).name();
            switch (NamedEntities.CALL_TO_ACTION.valueOf(actionCall))
            {
                //drop everything between "remind (me|us|*) " and "to"
                case REMIND_ME_TO:
                case TELL_ME_TO:
                {
                    //"remind me to" --> action: "remind to"
                    //string = string.replaceAll("(?<=remind|tell)(.*)(?=(to|that|about))" , " ");

                    //"remind me to" --> action: "remind me" , i.e. "remind Sally to" --> "remind Sally"
                    string = string.replaceAll("\\s+(to|that|about)", "");
                    break;
                }
                case DONT_FORGET_TO:
                case REMINDER_TO:
                case REMEMBER_TO:
                {
                    //picks text between "remember" and "to"
                    string = string.replaceAll("(?<=remember|reminder|don't forget)(.*)(?=(to|that|about))", " ");
                    break;
                }
            }
            this.callToAction = string.trim();
            altered = true;
        }
        else if (rule instanceof NamedEntities.MESSAGE_BODY)
        {
            String ruleName = ((NamedEntities.MESSAGE_BODY) rule).name();
            switch (NamedEntities.MESSAGE_BODY.valueOf(ruleName))
            {
                case AFTER_ABOUT_THAT_TO:
                    altered = MessageBodyExtractor.parse(this, string);
                    break;
            }
        }
        else if (rule instanceof NamedEntities.TIME_SHIFT)
        {
            String ruleName = ((NamedEntities.TIME_SHIFT) rule).name();
            switch (NamedEntities.TIME_SHIFT.valueOf(ruleName))
            {
                case IN_N: //in 5 minutes,...
                    haveTime = TimeShiftParser.parse(this, string);
                    altered = true;
                    break;
                case NEXT_N: // "next week" , "next year", "within 5 weeks", "within few minutes" ...
                    haveDay = TimeShiftDirect.parse(this, string);
                    altered = true;
                    break;
            }
        }
        else if (rule instanceof NamedEntities.TIME_PLACEMENT)
        {
            String ruleName = ((NamedEntities.TIME_PLACEMENT) rule).name();
            switch (NamedEntities.TIME_PLACEMENT.valueOf(ruleName))
            {
                case TIME_OF_DAY: //at 5:30 (P.M.|in the morning|etc)*
                    haveTime = TimeParser.parse(this, string);
                    altered = true;
                    break;

                case DETERMINISTIC_DAY: //tomorrow, at lunch, after tomorrow,...
                    haveDay = DeterministicDayParser.parse(this, string);
                    altered = true;
                    break;

                case AMBIGUOUS_TIME: //in the morning, this evening, tonight,...
                        haveTime = AmbiguousTimeParser.parse(this, string);
                        altered = true;
                    break;

                case DAY_OF_WEEK: //on Monday, next Tuesday,...
                    /**
                     * This check is needed for cases like "remind me tomorrow night about the meeting on friday"
                     * TODO: need to allow override is DOW follows call to action, i.e. "Somehting important this Sunday, remind me on Thursday"
                     */
                    if (!haveDay) {
                        haveDay = DayOfWeekParser.parse(this, string);
                        altered = true;
                    }
                    break;

                case MONTH_OF_YEAR: //in February, next August,...
                    haveDay = MonthOfYearParser.parse(this, string);
                    altered = true;
                    break;

                case DAY_OF_MONTH: //on August 15th, 2014,...
                    haveDay = DayOfYearParser.parse(this, string);
                    altered = true;
                    break;

                case OF_DAY_OF_MONTH:
                    haveDay = DayOfYearParser.parse(this, string);
                    altered = true;
                    break;
            }
        }
        else if (rule instanceof NamedEntities.TIME_OFFSET)
        {
            String ruleName = ((NamedEntities.TIME_OFFSET) rule).name();
            switch (NamedEntities.TIME_OFFSET.valueOf(ruleName))
            {
                case BY_DOW: // remind me *the night before* that...
                    //calculate and store time offset to be applied as the last step before reminder is retrieved
                    DeterministicOffsetParser.parse(this, string);
                    altered = true;  //<--strips processed text from utterance
                    break;
                case BY_TIME:// remind me *30 minutes early*
                    //calculate and store time offset to be applied as the last step before reminder is retrieved
                    QuantativeOffsetParser.parse(this, string);
                    altered = true;  //<--strips processed text from utterance
                    break;
            }
        }
        else if (rule instanceof NamedEntities.RECURRING)
        {
            String ruleName = ((NamedEntities.RECURRING) rule).name();
            switch (NamedEntities.RECURRING.valueOf(ruleName))
            {
                case REPEAT_EVENT:
                    altered = RecurringParser.parse(this, string);
                    break;
                case DAYS:
                    altered = RecurringParser.parseDays(this, string);
                    break;
            }
        }

        return /*(haveTime || haveDay) &&*/ altered;
    }

    public void refreshStatus() {
        Calendar now = Calendar.getInstance();
        if (date.after(now)) {
            if (callToAction != null)
                status = ReminderStatus.FUTURE;
            else
                status = ReminderStatus.NO_REMINDER_DETECTED;
        }
        else if (date.before(now))
            status = ReminderStatus.LAPSED;
        else
            status = ReminderStatus.INVALID;
    }

    public void checkIsTimeUpdate()
    {
        Pattern p = Pattern.compile(TimeParser.REGEX_TIME_HOUR_AND_MINUTES, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        boolean found = m.find();
        if (found)
        {
            String time = text.substring(m.start(), m.end());
            int hr, mn = 0;

            String[] parts = time.split(":");
            hr = Integer.parseInt(parts[0]);
            if (parts.length == 2)
                mn = Integer.parseInt(parts[1]);

            if (hr>=0 && hr<=24 && mn>=0 && mn<60) {
                if (!text.contains(TimeParser.PM) && !text.contains(TimeParser.PM_) && !text.contains(TimeParser.AM) && !text.contains(TimeParser.AM_))
                    ampm = TimeParser.determineIntendedTimeOfDay(hr, mn, this);

                if (text.endsWith(TimeParser.PM) || text.endsWith(TimeParser.PM_) || ampm == Calendar.PM)
                    hr += 12;

                hour = hr;
                minute = mn;

                date.set(Calendar.HOUR_OF_DAY, hr);
                date.set(Calendar.MINUTE, mn);

                hint.append(Reminder.Hint.TIME_UPDATE);
            }
        }
    }

    /** TO-DO: this is awkward, use ObjectMapper instead.
     */
    public String getJson() {
        String jsonRequest  = "{";
        jsonRequest += " \"type\" : \"reminder\" , ";
        jsonRequest += " \"status\" : \""   + status        + "\" , ";
        jsonRequest += " \"hint\" : \""     + hint.getAll()    + "\" , ";
        jsonRequest += " \"weekday\" : \""  + CalendarUtils.getDOWAsString(date.get(Calendar.DAY_OF_WEEK)) + "\" , ";
        jsonRequest += " \"year\" : \""     + year          + "\" , ";
        jsonRequest += " \"month\" : \""    + month         + "\" , ";
        jsonRequest += " \"day\" : \""      + day           + "\" , ";
        jsonRequest += " \"hour\" : \""     + hour          + "\" , ";
        jsonRequest += " \"minute\" : \""   + minute        + "\" , ";
        jsonRequest += " \"utcdate\": \""   + utcTime       + "\" , ";
        jsonRequest += " \"timeZone\": \""  + clientTimeZone + "\" , ";
        jsonRequest += " \"action\" : \""   + (callToAction==null?"":callToAction)  + "\" , ";
        jsonRequest += " \"body\" : \""     + text          + "\" , ";
        jsonRequest += " \"utterance\" : \""+ utterance     + "\" , ";
        jsonRequest += " \"recurring\" : \""+ isRecurring() + "\" , ";
        if (recurring) {
            jsonRequest += " \"repeat\" : \"" + repeatTarget.name() + "," + repeatFrequency + "\" , ";
        }
        jsonRequest += " \"reqtime\" : \""  + processTime   + "\"   ";
        jsonRequest += "}";
        return jsonRequest;
    }

    public String getJsonPretty() {
        String jsonRequest  = "{\n";
        jsonRequest += " \t\"type\" : \"reminder\" , \n";
        jsonRequest += " \t\"status\" : \""   + status        + "\" , \n";
        jsonRequest += " \t\"hint\" : \""     + hint.getAll()    + "\" , \n";
        jsonRequest += " \t\"weekday\" : \""  + CalendarUtils.getDOWAsString(date.get(Calendar.DAY_OF_WEEK)) + "\" , \n";
        jsonRequest += " \t\"year\" : \""     + year          + "\" , \n";
        jsonRequest += " \t\"month\" : \""    + month         + "\" , \n";
        jsonRequest += " \t\"day\" : \""      + day           + "\" , \n";
        jsonRequest += " \t\"hour\" : \""     + hour          + "\" , \n";
        jsonRequest += " \t\"minute\" : \""   + minute        + "\" , \n";
        jsonRequest += " \t\"utcdate\": \""   + utcTime       + "\" , \n";
        jsonRequest += " \t\"timeZone\": \""  + clientTimeZone + "\" , \n";
        jsonRequest += " \t\"action\" : \""   + (callToAction==null?"":callToAction)  + "\" , \n";
        jsonRequest += " \t\"body\" : \""     + text          + "\" , \n";
        jsonRequest += " \t\"utterance\" : \""+ utterance     + "\" , \n";
        jsonRequest += " \t\"recurring\" : \""+ isRecurring() + "\" , \n";
        if (recurring) {
            jsonRequest += " \t\"repeat\" : \"" + repeatTarget.name() + "," + repeatFrequency + "\" , \n";
        }
        jsonRequest += " \t\"reqtime\" : \""  + processTime   + "\"   \n";
        jsonRequest += "}\n";
        return jsonRequest;
    }

    public String getJsonHtml() {
        String jsonRequest  = "<br/>{<br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">type</span>\" : \"reminder\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">status</span>\" : \""   + status        + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">hint</span>\" : \""     + hint.getAll()    + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">weekday</span>\" : \""  + CalendarUtils.getDOWAsString(date.get(Calendar.DAY_OF_WEEK)) + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">year</span>\" : \""     + year          + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">month</span>\" : \""    + month         + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">day</span>\" : \""      + day           + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">hour</span>\" : \""     + hour          + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">minute</span>\" : \""   + minute        + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">utcdate</span>\": \""   + utcTime       + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">timeZone</span>\": \""  + clientTimeZone + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">action</span>\" : \""   + (callToAction==null?"":callToAction)  + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">body</span>\" : \""     + text          + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">utterance</span>\" : \""+ utterance     + "\" , <br/>";
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989cd\">recurring</span>\" : \""+ isRecurring() + "\" , <br/>";
        if (recurring) {
            jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989ee\">repeat</span>\" : \"" + repeatTarget.name() + "," + repeatFrequency + "\" , <br/>";
        }
        jsonRequest += " &nbsp; &nbsp; &nbsp; \"<span style=\"color:#8989ee\">reqtime</span>\" : \""  + processTime   + "\"   <br/>";
        jsonRequest += "}<br/>";
        return jsonRequest;
    }

    private String isRecurring() {
        return recurring ? "yes" : "no";
    }

    public class FinalOffset {
        public int minutes = 0;
        public int hours   = 0;
        public int days    = 0;
        public int weeks   = 0;
        public int months  = 0;
        public int years   = 0;

        public int minutesDiff = 0;
        public int hoursDiff   = 0;
        public int daysDiff    = 0;
        public int weeksDiff   = 0;
        public int monthsDiff  = 0;
        public int yearsDiff   = 0;

        public void clear() {
            days    = 0;
            hours   = 0;
            minutes = 0;
            weeks   = 0;
            months  = 0;
            years   = 0;

            daysDiff    = 0;
            hoursDiff   = 0;
            minutesDiff = 0;
            weeksDiff   = 0;
            monthsDiff  = 0;
            yearsDiff   = 0;
        }
    }
}
