package com.gorny.nlp;

public class Main {

    public static void main(String[] args) {
        StringBuilder arguments = new StringBuilder();
        for(String arg:args) {
            arguments.append(arg).append(" ");
        }
        String utterance = arguments.toString().trim();
        System.out.println("To be processed: \"" + utterance + "\"");

        long startTs = System.currentTimeMillis();

        nlpProcessor nlp = new nlpProcessor();
        Reminder reminder = nlp.detect(utterance);

        long endTs = System.currentTimeMillis();
        reminder.processTime = endTs - startTs;

        System.out.println("Reminder status="+reminder.status+", hint="+reminder.hint.getAll()+" date=" + reminder.year + "-" + reminder.month + "-" + reminder.day + " @ " + reminder.hour + ":" + reminder.minute +
        ", call to action="+(reminder.callToAction==null?"null":"\""+reminder.callToAction+"\"") +
        ", body=\""+reminder.text+"\" , completed in "+reminder.processTime+" ms.\n\n"+reminder.getJsonPretty());
    }
}
